﻿Communication with regulator/dio has two solutions

//get data from regulators ! not started
            MotorsDataPooler = new Timer(new TimerCallback(PollAndSendSync), null, System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
One is parallel blocking  PollAndSendSync, 
can be made in to a non blocking task by surrounding it with task
Task.Run(()=>{whole code}); 

ParallelLoopResult result = Parallel.ForEach(ModBusConnections, worker => resultCollection.Add(worker.Value.PollMotorData()));
Inside there is a call to PollMotorData() 
Two points of interest inside are
bytesFromEmerson = RegulatorOutputs.ReadHoldingRegistersBytes(StartingRegister, Convert.ToUInt16(Properties.Settings.Default.NumberOfParametersInMenu0*2), this);
bool[] DioBit = Dio.ReadCoils(1, (ushort)1, 1);
they are of blocking type.

Other is taks async nonblocking PollAndSendAsync
It has a Task type

foreach (MotorIO sys in ModBusConnections.Values)
            {
                tasks.Add(sys.PollMotorDataAsync());
            }

            var results = await Task.WhenAll(tasks);
We wait for all results from non blocking  PollMotorDataAsync

Points of interest inside function are
bytesFromEmerson = await RegulatorOutputs.ReadHoldingRegistersAsyncBytes(StartingRegister, Convert.ToUInt16(Properties.Settings.Default.NumberOfParametersInMenu0 * 2));

bool[] DioBit = await Dio.ReadCoilsAsync(1, (ushort)1, 1);

Different approaches are used primarily for optimization and resource handling.
Parallel processing resource use spikes at some point and continues upscaling with curve almost flat. Amd has processors with lots of cores. Tasks are processor and memory hungry always. It upscaling curve never flats out. But for 30 to 40 tasks, standard processor may be sufficient.
