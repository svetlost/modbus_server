
# Server Modbus application 
Console application that has udp connection  to client side, and Modbus connection to Regulator and Dio. 
It has connection to Microsoft SQL database where it gets data from motor_list, which holds regulator and dio configurations, and code_words data table which holds info about used regulator registers.

Works through two timers:

-	MotorsDataPooler - used for polling data from regulators and dio. 
Tick is defined in MotorsDataPoolerInterval, also it has a mechanism that prevents polling in less then interval time.
Data is located on regulator menu zero.
Here you choose between parallel sync (PollAndSendSync) or Tasks async (PollAndSendAsync), details in separate fail.

-	ReconnectionTimer - used for reconnecting regulators and dios, it run in endless loop every  (ErroredMotorsReconnectionInterval) milliseconds if it detects connection loss.

Receives commands from client through udp (UDPClientCommandsForMotors).


