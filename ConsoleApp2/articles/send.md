Commands properties are red from database table code_words.
Code description is used as parameter command.

Data structure:
(4)[0] - Number of motors  1byte;
(0,1,2,3)[1- x range] - List of motors ids we are sending
(32)[x+1 - x+2] – type of command
-	8 send to dio
-	16 or 32 send to regulator  (as 16bit or 32bit message)
-	64 send multiple regulators 
*[ dioBit] – if DIO just dio bit value,
(5)[registerLength] – number of bits in command (16 or 32bits for regulator one register, or value is double )
command code to byte array ( we get "Amc Selected Reference", transform it to “33.006”,

Rest is :
[commandLength] – we need to send the length of the string as offset to start reading from correct position
[command] – in byte array
[value] – value to send 

Two functions send :
ModBusSigleCommandWrite() – for single command send, can be to single or multiple motors 
Has a special command named “DIO” and “SingleDIOCommandGroupedInit”. Both are command type 8;

ModbusGroupWrite()
Command type ids always -64
Commands as array 
commands = new string[] { "Run", "Set Position", "Amc Selected Reference", "DriveEnable", "Temp Velocity", "amc absolute MAX speed" };

receiving class for message on server side is RegulatorMessage
it has code , value and SEND().
