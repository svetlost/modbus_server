We poll data from regulator and dio. What we poll is defined in parameters menu 0 on regulator .
(parameter for starting point in regulator menu 0 StartingReadingRegisterMenu0)

For regulator we use ReadHoldingRegistersBytes. 

Parameters (On server, defined by Settings.NumberOfParametersInMenu0 (14) and Settings.StartParameterNumberInMenu22(22060)) are all polled as if they were 32bit. Since maximum size of registers on regulator are 16bit, we multiply poll number by two. On error number 32 is returned.

Dio is polled as bool ReadCoils. On error returns number 64.

Regulator has registers that are 16bits maximum. It can read 32bit and reads it as two consecutive registers. We read registers of various sizes, so read all as 32bit.
Exp. If we have 28 registers, that we read all as 32bit number of bytes here will by 28*2=56. So whole message will be 61 bytes long. 

Data structure as byte[]:

(example):

(4) - number of motors returning 1byte ( max is 256 motors, since we reserve 1 byte for it)

(1-5) - motors ids (red from db MotorID) 1bye each

(6-10) - length of messages for each motor , error message is one byte

(11-56*4) - messages, what we receive from regulator and dio,  first byte is always dio! 

On error we return on 6th byte:
(6) - 32,64 or 32+64=96;

PollAndSendSync () or  PollAndSendAsync() is used for sending data by udp to client.

First is used with parallel foreach, second is async task. Documented in code. 

MotorsDataPoolerInterval is set to 50 ms, it corresponds to clients ModbusRXPollerInterval also 50ms

Client Rx side is done in timer ReceivingMessagesOverUDP, uses parallel for filing MotorStatuses() with data. Documented in code. 
