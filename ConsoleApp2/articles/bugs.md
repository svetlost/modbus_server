﻿Bug

It is cue related. We cue is running if one of motors has an error the rest should stop. They don’t.
In Play()function in CuesLogic.cs  motors are sent to solo state machine  states.AutoStarting1;.
foreach (CueItem cueItem in CurrentCueItemsList)
            {
                MovementMotorsList.Add(cueItem.Motor);
                cueItem.Motor.soloSM.cueAuto(cueItem);                
            }

MainWindow.xaml.cs  void MainLoop_Tick

foreach (var obj in Sys.StaticMotorList)
 {
MotorLogic ml = obj.Value;
ml.CalculateEnableds();
ml.soloSM.ProcessStateMachine();
}

CalculateEnableds calculates state of each motor. 
ProcessStateMachine moves down the state machine.

There is GroupLogic class. Cues have to be rewritten to resemble that class.
Also there is 
foreach (GroupLogic gl in Sys.StaticGroupList){
gl.CalculateEnableds();
gl.groupSM.ProcessGroupStateMachine();
}

Since number of groups are defined in properties, cueLogic can’t be easily transformed. I suggest making a special case of group logic. It can loop  separatly or as part of Sys.StaticGroupList.

There is CalculateEnableds in CuesLogic finding out if cue can be started.
Bud there is no processing of movement (process state machine).
This would be the other kind of solution, adding processing to cue, that resembles group behavior.
