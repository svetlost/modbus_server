﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleModbusServer
{
    /// <summary>
    /// <c>Utilityes</c> class
    /// 
    /// </summary>
    public static class Utilz
    {
        /// <summary>
        /// converts to 16 bit or 32bit number
        /// if 32bit add 16384 yo number
        /// </summary>
        /// <param name="codeString">string to convert</param>
        /// <param name="bits">16 or 32 bits</param>
        /// <returns>ushort number for sending to regulators </returns>      
        public static ushort Ind(string codeString, Len bits)
        {
            CultureInfo cultures = new CultureInfo("en-US");
            string[] split = codeString.Split('.');
            int menu = Convert.ToInt32(split[0], cultures);
            int item = Convert.ToInt32(split[1], cultures);
            return (ushort)((menu * 100) + (item - 1) + (bits == Len.b32 ? 16384 : 0));
        }
        /// <summary>
        /// length in bytes
        /// </summary>
        public enum Len { b16, b32 }
        /// <summary>
        /// print out array of bytes
        /// </summary>
        /// <param name="bytes">array of bytes</param>
        /// <returns></returns>
        public static string PrintByteArray(byte[] bytes)
        {
            if (bytes == null)  { return "{}"; }
            var sb = new StringBuilder("{");
            for (int i = 0; i < bytes.Length; i++)
            {
                // if (bytes.Length - 1 == i) { sb.Append(bytes[i]); }2
                sb.Append(bytes[i] + ", ");
            }
            sb.Append("}");
            return sb.ToString();
        }
        /// <summary>
        /// creates a task that completes after specified number of milliseconds
        /// </summary>
        /// <param name="delay">number of milliseconds</param>
        /// <returns></returns>
        public static async Task PutTaskDelay(int delay)
        {
            await Task.Delay(delay).ConfigureAwait(true);
        }

        /// <summary>
        /// creating sub arrays
        /// </summary>
        /// <param name="data">array of bytes</param>
        /// <param name="index">starting position</param>
        /// <param name="length">number of bytes to read</param>
        /// <returns></returns>
        public static byte[] SubArray(byte[] data, int index, int length)
        {
            byte[] result = new byte[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        /// <summary>
        /// puts modbus connection id into error connections
        /// starts reconnection timer        
        /// don't kill all dios at the same time! program may be at many different returning dio points, random app kill
        /// </summary>
        /// <param name="regOrDio"></param>
        /// <param name="type">dio or regulator </param>
        public static void ErrorConnection(MotorIO regOrDio, string type)
        {
            string found;
            if (Connections.errorConnections.TryGetValue(regOrDio.ID, out found) && found == type) { return; }

            if (type == "dio")
            {

                regOrDio.Dio = null;
                regOrDio.DioConnected = false;

                // adds dio is not presented in error list
                var errorKeysinModbus = from first in Connections.errorConnections.Keys.ToList()
                                        join second in Connections.ModBusConnections.Values.Where(q => q.DioIpAddress == regOrDio.DioIpAddress).ToList()
                                        on first equals second.ID
                                        into matches
                                        where matches.Any()
                                        select first;
                if (errorKeysinModbus.Any() == false)
                {
                    Console.WriteLine("DIO error,  DIO : " + regOrDio.DioIpAddress);
                    Connections.errorConnections.TryAdd(regOrDio.ID, type);
                }
               

            }
            if (type == "regulator")
            {
                regOrDio.RegulatorConnected = false;
                bool match = Connections.errorConnections.ContainsKey(regOrDio.ID);
                if (match == false)
                {
                    Connections.errorConnections.TryAdd(regOrDio.ID, type);
                    Console.WriteLine(" REGULATOR error,  DIO : " + regOrDio.DioIpAddress + "  REGULATOR: " + regOrDio.RegulatorIpAddress + " \n");
                }

                if (regOrDio.RegulatorInputs != null) { regOrDio.RegulatorInputs.Dispose(); }
                if (regOrDio.RegulatorOutputs != null) { regOrDio.RegulatorOutputs.Dispose(); }
            }



            if (Connections.ReconectionInterval == System.Threading.Timeout.Infinite)
            {
                int interval = Properties.Settings.Default.ErroredMotorsReconnectionInterval;
                //do it immediately, and redo it every interval ms
                Connections.ReconnectionTimer.Change(0, interval);
            }

        }

        static ushort refStop = Utilz.Ind(Properties.Settings.Default.AmcSelectedReferenceCode, Utilz.Len.b16);
        static ushort runStop = Utilz.Ind(Properties.Settings.Default.RunCode, Utilz.Len.b16);
        /// <summary>
        /// used if connection to client part of app is severed
        /// and dio is not working
        /// do not use for changes in connection state
        /// </summary>
        /// <param name="ipaddress">for regulator or  dio that is not working</param>
        /// <param name="regulatorError">determents do we send regulator or dio ip address , if true send regulator address</param>
        public static void EmergencyStop(string ipaddress = "", bool regulatorError = false)
        {
            List<MotorIO> lista;

            if (Connections.ModBusConnections.Count == 0) { return; }

            // for stopping dio of sent regulator ip address, namely one
            if (regulatorError)
            {
                lista = Connections.ModBusConnections.Values.Where(q => q.RegulatorIpAddress == ipaddress).ToList();

            }

            // dio error, list of all regulators with that dio ip
            if (!string.IsNullOrEmpty(ipaddress) && regulatorError == false)
            {
                lista = Connections.ModBusConnections.Values.Where(q => q.DioIpAddress == ipaddress).ToList();
            }

            //for stopping everything
            else if (string.IsNullOrEmpty(ipaddress) && regulatorError == false)
            {
                lista = Connections.ModBusConnections.Select(x => x.Value).Distinct().ToList();
            }
            else { lista = new List<MotorIO>(); }



            foreach (MotorIO connection in lista)
            {
                // stop all
                //dio dead
                if (regulatorError == false && connection.RegulatorInputs != null)
                {
                    connection.RegulatorInputs.WriteSingleRegisterAsync(refStop, (ushort)0); //amc ref select 
                }

                TimedAction.ExecuteWithDelay(new Action(delegate
                  {
                      // stop all
                      // dio dead
                      if (regulatorError == false && connection.RegulatorOutputs != null)
                      {
                          connection.RegulatorInputs.WriteSingleRegisterAsync(runStop, (ushort)0); //amc run stop
                      }

                      //stop all
                      //regulator dead
                      if (connection.Dio != null && regulatorError)
                      {
                          connection.Dio.WriteSingleCoil((byte)connection.DioModul, (ushort)connection.DioBit, false);
                      }
                  }), TimeSpan.FromMilliseconds(50));

            }
        }



        static Stopwatch sw = new Stopwatch();
        /// <summary>
        /// Async TCP client call to regulator 
        /// on error or timeout returns null
        /// </summary>
        /// <param name="targetIpAddress">regulator ip address to connect to</param>
        /// <returns>null or tcp client</returns>
        public static TcpClient TcpClientModbusConnection(string targetIpAddress)
        {

            try
            {
                TcpClient client = new TcpClient { ReceiveTimeout = Properties.Settings.Default.TcpConnectionTimeOut };

                var connectionTask = client.ConnectAsync(targetIpAddress, 502).ContinueWith(task => { return task.IsFaulted ? null : client; }, TaskContinuationOptions.ExecuteSynchronously);
                var timeoutTask = Task.Delay(Properties.Settings.Default.TcpConnectionTimeOut).ContinueWith<TcpClient>(task => null, TaskContinuationOptions.ExecuteSynchronously);

                //waits timeout time for tcp client to establish connection, returns null if time outs
                var resultTask = Task.WhenAny(connectionTask, timeoutTask).Unwrap();

                resultTask.Wait();
                var resultTcpClient = resultTask.Result;

                if (resultTcpClient != null)
                {
                    Console.WriteLine(targetIpAddress + " TCP Client successfully connected...");
                    return client;
                }
                else
                {
                    return null;
                }
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(sw.ElapsedMilliseconds + " ms  ---  Argument Null Exception: " + e.Message + " parameter name: " + e.Data);
                return null;
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(sw.ElapsedMilliseconds + " ms  ---  Argument Null Exception: " + e.Message + " parameter name: " + e.ParamName);
                return null;
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine(sw.ElapsedMilliseconds + " ms  ---  Argument Out Of Range Exception: " + e.Message + " value: " + e.ActualValue);
                return null;
            }
            catch (ObjectDisposedException e)
            {
                Console.WriteLine(sw.ElapsedMilliseconds + " ms  ---  Object Disposed Exception: " + e.Message + " object name: " + e.ObjectName);
                return null;
            }
            catch (SocketException e)
            {
                //a socket error has occurred
                switch (e.SocketErrorCode)
                {
                    case SocketError.TimedOut:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Time Out " + targetIpAddress +"\n"+e.StackTrace);
                        break;
                    case SocketError.WouldBlock:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Would Block " + targetIpAddress + "\n" + e.StackTrace);
                        break;
                    case SocketError.ConnectionReset:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Connection Reset " + targetIpAddress + "\n" + e.StackTrace);
                        break;
                    case SocketError.ConnectionAborted:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Connection Aborted " + targetIpAddress + "\n" + e.StackTrace);
                        break;
                    case SocketError.NotConnected:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Not Connected " + targetIpAddress + "\n" + e.StackTrace);
                        break;
                    case SocketError.HostUnreachable:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Host Unreachable " + targetIpAddress + "\n" + e.StackTrace); // izvucen kabl iz kompa   ili nije ukljucen       
                        break;
                    case SocketError.NetworkUnreachable:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Network Unreachable " + targetIpAddress + "\n" + e.StackTrace); // izvucen kabl iz kompa pa vracen, 
                                                                                     //    Thread.Sleep(25000); // test add on
                        break;
                    case SocketError.ConnectionRefused:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Connection Refused " + targetIpAddress + "\n" + e.StackTrace);
                        break;
                    default:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, sw.ElapsedMilliseconds + " ms  ---  No Answer Received: " + e.Message + " " + e.SocketErrorCode);
                        //     Thread.Sleep(1000); // test add on
                        break;
                }
                return null;
            }
        }
       
        /// <summary>
        /// CHECK LIST ERROR check if regulator and db have same values in the same order, checks only on first sync.
        ///  this has to be done after communication checks 
        /// </summary>
        public static bool RegulatorCommandsListCheck()
        {
            int errorcounter = 0;
            try {
                for (int i = Connections.ModBusConnections.Count - 1; i >= 0; i--)
                {
                    var connection = Connections.ModBusConnections.ElementAt(i).Value;

                    if (connection.ComapreDBRegulatorCommands && connection.DioConnected && connection.RegulatorConnected)
                    {

                        for (var count = 0; count < Connections.codeList.Count; count++)
                        {
                            var nfi = new NumberFormatInfo { NumberDecimalSeparator = ",", NumberGroupSeparator = "." };

                            //read from starting position parameter in the list in regulator menu 22 (Menu 0 set-up)
                            int StartingPositionForCommands = Properties.Settings.Default.StartParameterNumberInMenu22 + count;

                            string registerNumber = StartingPositionForCommands.ToString("#,##", nfi); //result will always be as 1.234

                            //starting register
                            ushort StartingRegister1 = Utilz.Ind(registerNumber, Utilz.Len.b16);

                            //read  parameter value
                            byte[] bytesFromEmerson1 = connection.RegulatorOutputs.ReadHoldingRegistersBytes(StartingRegister1, 1);
                            Array.Reverse(bytesFromEmerson1, 0, bytesFromEmerson1.Length);

                            int RegulatorCode = BitConverter.ToUInt16(bytesFromEmerson1, 0);

                            if(!Connections.codeList[count].Contains("."))
                            {
                                LogWriter.LogWrite(LogWriter.Types.Fatal, $"ERROR ... table code_words with value {Connections.codeList[count]} does not contain dot(.) separator");
                                Console.WriteLine("Error in regulator command list check see in logs..");
                            }

                            if((Connections.codeList[count].Length - Connections.codeList[count].IndexOf(".", StringComparison.CurrentCulture) - 1) != 3)
                            {
                                LogWriter.LogWrite(LogWriter.Types.Fatal, $"ERROR ... table code_words with value {Connections.codeList[count]} does not contain three digits after dot(.) separator");
                                Console.WriteLine("Error in regulator command list check see in logs..");
                            }

                            //convert code from db to int
                            string cleanAmount = Connections.codeList[count].Replace(".", string.Empty);                            

                            int DBcodeNumber = int.Parse(cleanAmount, CultureInfo.CurrentCulture);


                            if (DBcodeNumber != RegulatorCode)
                            {
                                errorcounter++;
                                LogWriter.LogWrite(LogWriter.Types.Fatal, "ERROR ... Code index number (start from zero) :  " + count + " ---> in Regulator it is :" + RegulatorCode + "  in DB it is :" + Connections.codeList[count] + " for Regulator on address:" + connection.RegulatorIpAddress);
                            }

                            if (count == Connections.codeList.Count - 1)
                            {
                                connection.ComapreDBRegulatorCommands = false;
                                if (errorcounter == 0)
                                {
                                    LogWriter.LogWrite(LogWriter.Types.Info, "CHECK if Regulator: " + connection.RegulatorIpAddress + " (menu 0) and DB code_words table are the same : " + " TRUE ");
                                }
                                else
                                {
                                    LogWriter.LogWrite(LogWriter.Types.Error, "CHECK if Regulator: " + connection.RegulatorIpAddress + " (menu 0) and DB code_words table are the same : " + " FALSE ");
                                }
                                errorcounter = 0;
                            }
                        }
                        Console.WriteLine("\n");
                       
                    }
                }
                return true;
            }
             catch (Exception ex)
            {
                LogWriter.LogWrite(LogWriter.Types.Fatal, "CHECK Regulator menu 22 and check database table code_words, they have to be the same");
                LogWriter.LogWrite(LogWriter.Types.Fatal, ex.Message);
                LogWriter.LogWrite(LogWriter.Types.Fatal, ex.StackTrace);
                return false;
            }
        }
    }
}
