﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleModbusServer
{
    /// <summary>
    ///  <c>MotorIO</c> Class that has establishes regulator and dio modbus tcp connections. It handles reconnections.
    ///  </summary>  
    ///  <remarks>
    /// one regulator ModbusIpMaster input, for sending data to regulator
    /// one regulator ModbusIpMaster output for getting data from regulator
    /// for better performance regulator inputs and outputs are separated
    /// @DioConnected and @RegulatorConnected are connection statuses, but we check current status with .Connected()
    ///</remarks>
    public class MotorIO
    {
        private bool _comapreDBRegulatorCommands = true, _dioconnectionok = false, _regulatorconnectionok = false;
        private string _regulatoripAddress;
        private string _dioIpAddress;
        private ModbusIpMaster _regulatorsync, _regulatorio;
        private ModbusIpMaster _dio;
        private byte _dioBit, _initdioBit;
        private byte _dioModul;
        private int _id = -1;
        TcpClient resultTcpClient = null;

        /// <value>This is id from database motor_list</value>
        public int ID
        {
            get { return _id; } 
            set { _id = value; }
        }

        /// <value>This is Regulator Ip Address from database motor_list</value>
        public string RegulatorIpAddress
        {
            get { return _regulatoripAddress; }
            set { _regulatoripAddress = value; }
        }
        /// <value>This is Dio Ip Address from database motor_list</value>
        public string DioIpAddress
        {
            get { return _dioIpAddress; }
            set { _dioIpAddress = value; }
        }

        /// <summary>
        /// return of Dio.Connected();
        /// </summary>
        public bool DioConnected
        {
            get { return _dioconnectionok; }
            set { _dioconnectionok = value; }

        }
        /// <summary>
        /// return of RegulatorInputs.Connected() or RegulatorOutputs.Connected();
        /// </summary>
        public bool RegulatorConnected
        {
            get { return _regulatorconnectionok; }
            set { _regulatorconnectionok = value; }

        }

        /// <summary>
        /// Modbus IP regulator device used for polling current regulator state
        /// </summary>
        public ModbusIpMaster RegulatorOutputs
        {
            get { return _regulatorsync; }
            set { _regulatorsync = value; }
        }
        /// <summary>
        ///Modbus IP regulator device used for sending commands to regulator
        /// </summary>
        public ModbusIpMaster RegulatorInputs
        {
            get { return _regulatorio; }
            set { _regulatorio = value; }
        }
        /// <summary>
        ///Modbus IP regulator device used for polling current DIO state
        /// </summary>
        public ModbusIpMaster Dio
        {
            get { return _dio; }
            set { _dio = value; }
        }
        /// <value>This is Dio Bit from database motor_list</value>
        public byte DioBit
        {
            get { return _dioBit; }
            set { _dioBit = value; }
        }
        /// <value>This is Dio Bit for initialization from database motor_list</value>
        public byte InitDioBit
        {
            get { return _initdioBit; }
            set { _initdioBit = value; }
        }
        /// <value>This is Dio Modul from database motor_list</value>
        public byte DioModul
        {
            get { return _dioModul; }
            set { _dioModul = value; }
        }
        /// <remark>Register in regulator from which list of reading parameters starts</remark>
        ushort StartingRegister = Utilz.Ind(Properties.Settings.Default.StartingReadingRegisterMenu0, Utilz.Len.b32);

        /// <summary>
        /// compares regulator list of commands and db list of commands
        /// list starting position for regulator is in properties.settings.StartAddressMenu22 is parameter in menu 22
        /// </summary>
        public bool ComapreDBRegulatorCommands
        {
            get { return _comapreDBRegulatorCommands; }
            set { _comapreDBRegulatorCommands = value; }
        }

        /// <summary>
        /// Connects to regulator and dio;
        ///  Makes two separate connections to regulator, one is used for pooling data from menu 0, other for sending commands from client to regulator;
        ///  Checks if dio connection already exists, if doesn't creates new connection, if does reference existing;
        ///  adds this to ModBusConnections dictionary
        ///  Creates errorConnections on error
        /// </summary>
        ///<param name="id">motor id,from db</param>
        /// <param name="regulatorip">regulator ip,from db</param>
        /// <param name="dioip">dio ip,from db</param>
        /// <param name="diomodul">dio module,from db</param>        
        /// <param name="initdiobit">initDio bit,from db</param>
        /// <param name="diobit">dio bit,from db</param>
        public MotorIO(int id, string regulatorip, string dioip, byte diomodul, byte initdiobit, byte diobit)
        {
            ID = id;
            RegulatorIpAddress = regulatorip;
            DioIpAddress = dioip;
            DioBit = diobit;
            DioModul = diomodul;
            InitDioBit = initdiobit;

            var match = Connections.ModBusConnections.Values.FirstOrDefault(q => q.DioIpAddress == dioip);
            if (match == null )
            {
                Dio = ModbusTcpConnections(RegulatorIpAddress, DioIpAddress, "dio");
                if (Dio != null)
                    DioConnected = Dio.Connected();
            }
            else
            {
                if (Dio == null)
                {
                    Dio = ModbusTcpConnections(RegulatorIpAddress, DioIpAddress, "dio");
                    if (Dio != null)
                        DioConnected = Dio.Connected();
                }
                else
                {
                    Dio = match.Dio;
                    DioConnected = match.DioConnected;
                }
                
            }
            //ModbusIpMaster or null 
            RegulatorOutputs = ModbusTcpConnections(RegulatorIpAddress, DioIpAddress, "regulator", "Output");
            //ModbusIpMaster or null 
            RegulatorInputs = ModbusTcpConnections(RegulatorIpAddress, DioIpAddress, "regulator", "Input");

            if (RegulatorOutputs != null) { RegulatorConnected = RegulatorOutputs.Connected(); }
            else { RegulatorConnected = false; }

            Connections.ModBusConnections.TryAdd(this.ID, this);

            //error checks
            if (Dio == null || DioConnected == false) { Utilz.ErrorConnection(this, "dio"); }
            if (RegulatorOutputs == null || RegulatorConnected == false) { Utilz.ErrorConnection(this, "regulator"); }

            LogWriter.LogWrite(LogWriter.Types.Info, String.Format("Dio: " + DioIpAddress + " is Connected: " + ((Dio == null) ? "false" : Dio.Connected().ToString()) + " Regulator: " + RegulatorIpAddress + " is Connected: " + ((RegulatorOutputs == null) ? "false" : RegulatorOutputs.Connected().ToString())));

        }
        /// <summary>
        /// Reconnects to dio or regulator;
        ///  Finds all regulator/dio connections with same dio;
        ///  Only one dio is needed, rest of regulators connected to same dio are referenced 
        /// </summary>
        /// <param name="type">regulator or dio</param>
        public void Reconnect(string type)
        {
            if (type == "dio")
            {
                Dio = ModbusTcpConnections(RegulatorIpAddress, DioIpAddress, "dio");

                string bla;
                if (Dio != null)
                {
                    DioConnected = Dio.Connected();

                    //finds all regulators with this dio
                    List<MotorIO> dioList = Connections.ModBusConnections.Values.Where(q => q.DioIpAddress == DioIpAddress).ToList();
                    foreach (MotorIO m in dioList)
                    {
                        Console.WriteLine("DIO at: {0}  {1}", m.RegulatorIpAddress, (DioConnected == true) ? "connected" : "not connected");
                        if (m.RegulatorIpAddress != this.RegulatorIpAddress)
                        {
                            m.Dio = this.Dio;
                            m.DioConnected = this.Dio.Connected();
                            if (m.DioConnected)
                                Connections.errorConnections.TryRemove(m.ID, out bla);
                        }
                        else
                        {
                            if (DioConnected)
                                Connections.errorConnections.TryRemove(ID, out bla);
                        }
                    }
                }
            }
            else
            {
                RegulatorOutputs = ModbusTcpConnections(RegulatorIpAddress, DioIpAddress, "regulator", "Output");
                RegulatorInputs = ModbusTcpConnections(RegulatorIpAddress, DioIpAddress, "regulator", "Input");
                if (RegulatorOutputs != null)
                {
                    RegulatorConnected = RegulatorOutputs.Connected();
                }
                else
                {
                    RegulatorConnected = false;
                }
            }
        }

        /// <summary>
        /// Creates or takes existing modbus tcp connection (ModbusIpMaster) for dio or regulator
        /// pings regulator
        /// </summary>
        /// <param name="ip">address</param>
        /// <param name="dioip">address</param>
        /// <param name="type">regulator or dio</param>      
        ///<param name="regulatorjob">Input or Output tcp modbus connection</param> 
        /// <returns>null on error or ModbusIpMaster</returns>
        public ModbusIpMaster ModbusTcpConnections(string ip, string dioip, string type, string regulatorjob = "")
        {
            try
            {
                string targetIpAddress = string.Empty;

                MotorIO existingConnection = null;

                // type determents are we sending regulator or dio to connect to 
                if (type == "dio")
                {
                    targetIpAddress = dioip;
                    existingConnection = Connections.ModBusConnections.Values.FirstOrDefault(q => q.DioIpAddress == targetIpAddress);
                    if (existingConnection != null && (existingConnection.Dio == null || !existingConnection.Dio.Connected()))
                    {
                        existingConnection = null;
                    }
                }
                if (type == "regulator")
                {
                    targetIpAddress = ip;
                    if (string.IsNullOrEmpty(regulatorjob))
                    {
                        existingConnection = Connections.ModBusConnections.Values.FirstOrDefault(q => q.RegulatorIpAddress == targetIpAddress);
                        if (existingConnection != null && (existingConnection.RegulatorOutputs == null || !existingConnection.RegulatorOutputs.Connected()))
                        {
                            existingConnection = null;
                        }
                    }
                    else { existingConnection = null; }

                }               

                // can we reach target address
                using Ping ping = new Ping();

                IPAddress address = IPAddress.Parse(targetIpAddress);
                PingReply pong = ping.Send(address, 500);


                // we try to ping regulator or dio
                if (pong.Status == IPStatus.Success)
                {

                    Console.WriteLine(targetIpAddress + " --> " + type + " " + regulatorjob + " : Ping Success");
                    LogWriter.LogWrite(LogWriter.Types.Info, targetIpAddress + " --> " + type + " " + regulatorjob + " : Ping Success");

                    // we were already connected
                    if (existingConnection != null)
                    {
                        Console.WriteLine(type + " " + regulatorjob + " on address: " + targetIpAddress + " is already in pool, referencing existing TCP client");
                        LogWriter.LogWrite(LogWriter.Types.Info, type + " " + regulatorjob + " on address: " + targetIpAddress + " is already in pool, referencing existing TCP client");

                        //if dio is connected
                        if (type == "dio")
                        {
                            return existingConnection.Dio;
                        }
                        else
                        {
                            if (regulatorjob == "Output")
                                return existingConnection.RegulatorOutputs;
                            else
                                return existingConnection.RegulatorInputs;
                        }
                    }
                    // new connection is required
                    else
                    {
                        resultTcpClient = Utilz.TcpClientModbusConnection(targetIpAddress);
                        //TcpClient resultTcpClient = new TcpClient(targetIpAddress,502);


                        if (resultTcpClient != null)
                        {

                            //sw.Restart();                      

                            return ModbusIpMaster.CreateIp(resultTcpClient);
                        }
                        else
                        {
                            Console.WriteLine("ERROR..." + type + " at address: " + targetIpAddress + " fails to connect in 1000 ms");
                            LogWriter.LogWrite(LogWriter.Types.Error, "ERROR..." + type + " at address: " + targetIpAddress + " fails to connect in 1000 ms");
                            Utilz.ErrorConnection(this, type);
                            return null;

                        }
                    }
                }
                else
                {
                    Console.WriteLine("ERROR..." + type + " at address: " + targetIpAddress + " ping status: " + pong.Status);
                    LogWriter.LogWrite(LogWriter.Types.Error, "ERROR..." + type + " at address: " + targetIpAddress + " ping status: " + pong.Status);
                    return null;
                }

            }
            catch (ArgumentNullException e)
            {
                LogWriter.LogWrite(LogWriter.Types.Fatal, "Argument Null Exception: " + e.Message + " parameter name: " + e.ParamName + " " + e.StackTrace);
                return null;
            }
            catch (InvalidOperationException e)
            {
                LogWriter.LogWrite(LogWriter.Types.Fatal, "---Invalid Operation Exception--- \n Message: " + e.Message + "\n\n Stack Trace: " + e.GetBaseException().StackTrace + "\n\nInner Exception: " + e.InnerException + "\n\n");
                return null;
            }
            catch (NotSupportedException e)
            {
                LogWriter.LogWrite(LogWriter.Types.Fatal, "Not Supported Exception: " + e.Message + " Stack Trace: " + e.GetBaseException().StackTrace);
                return null;
            }

        }

       

        /// <summary>
        /// Reads and returns parameters from regulator menu 0 and dio; 
        /// </summary>
        /// <remarks>Starts reading from: Properties.Settings.Default.StartingRegister
        ///  number of parameters to read in Properties.Settings.Default.NumberOfParameters 
        ///  reads dio status
        ///  checks connection statuses before polling
        ///  on error creates ErrorConnection         /// 
        /// 32 regulator error
        /// 64 dio error
        /// 96 regulator and dio error
        /// </remarks> 
        /// <returns>list of bytes from regulator and dio</returns> 
        /// <remarks>on error 32, 64 or 96, 32 64 96 have nothing to do with register bits! they are just numbers</remarks>
        public byte[] PollMotorData()
        {
            
            int returnerrorint = 0;
           
            try
            {
                List<byte> list = new List<byte>();
                byte[] bytesFromEmerson = { 0x20 }; // 32 is error code;

                byte[] id = BitConverter.GetBytes(ID);
                //Array.Reverse(id);
                list.AddRange(id);

                //CONECTION ERROR has been previously detected
                if (DioConnected == false) { returnerrorint +=  64; }//dio error 
                if (RegulatorConnected == false) { returnerrorint +=  32; } //regulator error 
                if (returnerrorint != 0)
                {
                    list.AddRange(BitConverter.GetBytes(returnerrorint));
                    return list.ToArray();
                }


                // check momentary status TCP SOCKET ERROR               
                if (this != null)
                {
                    if (Dio == null || Dio.Connected() == false)
                    {
                        Utilz.ErrorConnection(this, "dio");
                        //list.AddRange(new byte[] { 0x40 });  
                        returnerrorint += 64; //dio error
                    }
                    if (RegulatorOutputs == null || RegulatorOutputs.Connected() == false)
                    {
                        Utilz.ErrorConnection(this, "regulator");
                        //list.AddRange(new byte[] { 0x20 });
                        returnerrorint += 32; //regulator error
                    }

                    if (returnerrorint != 0)
                    {
                        list.AddRange(BitConverter.GetBytes(returnerrorint));
                        return list.ToArray();
                    }
                }
              
                 
               //reading all parameters from regulator as if they were 32bit (two registers, register in regulator are max 16bit), so multiply number of registers red by two 
                bytesFromEmerson = RegulatorOutputs.ReadHoldingRegistersBytes(StartingRegister, Convert.ToUInt16(Properties.Settings.Default.NumberOfParametersInMenu0*2), this);

                //REGULATOR ERROR HANDLING 
                // 32 is random number to prevent errors, safe since first return parameter in regulator parameter list is bool
                if (bytesFromEmerson.Length == 1 && bytesFromEmerson[0] == 32)
                {
                    Utilz.ErrorConnection(this, "regulator");
                    list.AddRange(new byte[] { 0x20 }); return list.ToArray();
                }

                //read from dio
                bool[] DioBit = Dio.ReadCoils(1, (ushort)1, 1);

                var workerEnd = DateTime.Now;
                //       Console.WriteLine("Worker {0} stopped; the worker took {1} Milliseconds, and it finished {2} Milliseconds after the test start.",
                //         RegulatorIpAddress, (workerEnd - workerStart).TotalSeconds.ToString("F2"), (workerEnd - testStart).TotalMilliseconds.ToString("F2"));

                byte[] fromDio = new byte[1];
                new BitArray(DioBit).CopyTo(fromDio, 0);

                // DIO ERROR HANDLING
                if (DioBit.Length == 8 && (int)fromDio[0] == 64)
                {

                    LogWriter.LogWrite(LogWriter.Types.Fatal, "GetRegulatorData dio null or 64: tried to read, connection established but no response from DIO on this ip: " + DioIpAddress + "\n\n");
                    Utilz.ErrorConnection(this, "dio");

                    list.AddRange(new byte[] { 0x40 }); return list.ToArray();
                }


                list.AddRange(fromDio);
                list.AddRange(bytesFromEmerson);
                return list.ToArray();

            }
            catch (ArgumentNullException ex)
            {
                LogWriter.LogWrite(LogWriter.Types.Fatal, "PollMotorData ArgumentNullException:" + ex.Message + " " + ex.StackTrace);
                return new byte[] { 0x20 };
            }
            catch (Exception ex)
            {
                LogWriter.LogWrite(LogWriter.Types.Fatal, "PollMotorData:" + ex.Message + " " + ex.StackTrace);
                return new byte[] { 0x20 };
            }
        }
        /// <summary>
        /// as  PollMotorData just async
        /// </summary>
        /// <returns>list of bytes</returns>
        public async Task<byte[]> PollMotorDataAsync()
        {

            int returnerrorint = 0;

            try
            {
                List<byte> list = new List<byte>();
                byte[] bytesFromEmerson = { 0x20 }; // 32 is error code;

                byte[] id = BitConverter.GetBytes(ID);
                Array.Reverse(id);
                list.AddRange(id);

                //CONECTION ERROR has been previously detected
                if (DioConnected == false) { returnerrorint += 64; }//dio error 
                if (RegulatorConnected == false) { returnerrorint += 32; } //regulator error 
                if (returnerrorint != 0)
                {
                    list.AddRange(BitConverter.GetBytes(returnerrorint));
                    return list.ToArray();
                }


                // check momentary status TCP SOCKET ERROR               
                if (this != null)
                {
                    if (Dio == null || Dio.Connected() == false)
                    {
                        Utilz.ErrorConnection(this, "dio");
                        //list.AddRange(new byte[] { 0x40 });  
                        returnerrorint += 64; //dio error
                    }
                    if (RegulatorOutputs == null || RegulatorOutputs.Connected() == false)
                    {
                        Utilz.ErrorConnection(this, "regulator");
                        //list.AddRange(new byte[] { 0x20 });
                        returnerrorint += 32; //regulator error
                    }

                    if (returnerrorint != 0)
                    {
                        list.AddRange(BitConverter.GetBytes(returnerrorint));
                        return list.ToArray();
                    }
                }


                // we are reading all parameters from regulator as if they were 32bit (two registers, register in regulator are max 16bit), so we multiply number of registers we are reading by two 
                bytesFromEmerson = await RegulatorOutputs.ReadHoldingRegistersAsyncBytes(StartingRegister, Convert.ToUInt16(Properties.Settings.Default.NumberOfParametersInMenu0 * 2)).ConfigureAwait(true);

                //REGULATOR ERROR HANDLING 
                // 32 is random number to prevent errors, safe since first return parameter is bool
                if (bytesFromEmerson.Length == 1 && bytesFromEmerson[0] == 32)
                {
                    Utilz.ErrorConnection(this, "regulator");
                    list.AddRange(new byte[] { 0x20 }); return list.ToArray();
                }

                bool[] DioBit = await Dio.ReadCoilsAsync(1, (ushort)1, 1).ConfigureAwait(true);

                var workerEnd = DateTime.Now;
                //       Console.WriteLine("Worker {0} stopped; the worker took {1} Milliseconds, and it finished {2} Milliseconds after the test start.",
                //         RegulatorIpAddress, (workerEnd - workerStart).TotalSeconds.ToString("F2"), (workerEnd - testStart).TotalMilliseconds.ToString("F2"));

                byte[] fromDio = new byte[1];
                new BitArray(DioBit).CopyTo(fromDio, 0);

                // DIO ERROR HANDLING
                if (DioBit.Length == 8 && (int)fromDio[0] == 64)
                {

                    Console.WriteLine("GetRegulatorData dio null or 64: tried to read, connection established but no response from DIO on this ip: " + DioIpAddress + "\n\n");
                    Utilz.ErrorConnection(this, "dio");

                    list.AddRange(new byte[] { 0x40 }); return list.ToArray();
                }
                list.AddRange(fromDio);
                list.AddRange(bytesFromEmerson);
                return list.ToArray();
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("Do Work ArgumentNullException:" + ane.Message + " " + ane.StackTrace);
                return new byte[] { 0x20 };
            }
            catch (Exception ex)
            {
                Console.WriteLine("Do Work :" + ex.Message + " " + ex.StackTrace);
                return new byte[] { 0x20 };
            }

        }


    }
}
