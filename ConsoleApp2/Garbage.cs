﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleModbusServer
{
    class Garbage
    {

        public static byte[] SubArray(byte[] data, int index, int length)
        {
            byte[] result = new byte[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
        public static byte[] AppendBytes(byte[] source, byte[] destination)
        {
            int i = destination.Length;
            Array.Resize<byte>(ref destination, i + source.Length);
            source.CopyTo(destination, i);

            return destination;
        }
        public static void SetBit(ref ushort b, int bitNumber, bool state)
        {
            //if (bitNumber < 0 || bitNumber > 15)//throw an Exception or return
            if (state) b |= (ushort)(1 << bitNumber);
            else { int i = b; i &= ~(1 << bitNumber); b = (ushort)i; }
        }

        public static bool GetBit(ushort b, int bitNumber)
        {
            //if (bitNumber < 0 || bitNumber > 15)   //throw an Exception or just return false
            return (b & (1 << bitNumber)) > 0;
        }

        public static string PrintByteArrayList(BlockingCollection<byte[]> bytes)
        {
            var sb = new StringBuilder("{");


            foreach (byte[] b in bytes)
            {
                sb.Append("{");
                for (int i = 0; i < b.Length; i++)
                {
                    if (b.Length - 1 == i) { sb.Append(b[i]); }
                    else { sb.Append(b[i] + ", "); }
                }
                sb.Append("}");
            }
            sb.Append("}");
            return sb.ToString();
        }

    

        public static async Task<TcpClient> IsConnected(string ipAddress, int port)
        {

            //Try to Connect with the host during 2 second
            try
            {
                // Create TcpClient and try to connect
                using (TcpClient client = new TcpClient())
                {
                    //Create Tasks
                    var clientTask = client.ConnectAsync(ipAddress, port);
                    var delayTask = Task.Delay(100);

                    //Check which one finish first
                    var completedTask = await Task.WhenAny(new[] { clientTask, delayTask });

                    //Check if the connection have been established before the end of the timer 
                    if (completedTask == clientTask)
                        return client;
                    else return null;
                }
            }
            catch (SocketException e)
            {
                //a socket error has occurred
                switch (e.SocketErrorCode)
                {
                    case SocketError.TimedOut:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Time Out " + ipAddress);
                        break;
                    case SocketError.WouldBlock:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Would Block " + ipAddress);
                        break;
                    case SocketError.ConnectionReset:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Connection Reset " + ipAddress);
                        break;
                    case SocketError.ConnectionAborted:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Connection Aborted " + ipAddress);
                        break;
                    case SocketError.NotConnected:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Not Connected " + ipAddress);
                        break;
                    case SocketError.HostUnreachable:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Host Unreachable " + ipAddress); // izvucen kabl iz kompa   ili nije ukljucen  
                        break;
                    case SocketError.NetworkUnreachable:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Network Unreachable " + ipAddress); // izvucen kabl iz kompa pa vracen, 
                                                                               //    Thread.Sleep(25000); // test addon
                        break;
                    case SocketError.ConnectionRefused:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "Connection Refused " + ipAddress);
                        break;
                    default:
                        LogWriter.LogWrite(LogWriter.Types.Fatal, "   ---  No Answer Received: " + e.Message + "\n" + e.SocketErrorCode + "\n" + e.StackTrace );
                        //     Thread.Sleep(1000); // test add on
                        break;
                }
                return null;
            }
        }

        /// <summary>
        /// Check if client is connected and that connection is the established state
        /// hack is used for not connected states
        /// </summary>
        /// <param name="client"></param>
        /// <returns>state of connection</returns>
        public static TcpState Tcpstate(TcpClient client)
        {
            if (client.Client == null) { return TcpState.Closed; }
            IPGlobalProperties ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint remoteIpEndPoint = client.Client.RemoteEndPoint as IPEndPoint;
            IPEndPoint localIpEndPoint = client.Client.LocalEndPoint as IPEndPoint;
            TcpConnectionInformation[] tcpConnections = ipProperties.GetActiveTcpConnections().Where(x => x.LocalEndPoint.Address.Equals(localIpEndPoint.Address) && x.RemoteEndPoint.Equals(remoteIpEndPoint) && x.State.Equals(TcpState.Established)).ToArray();

            if (tcpConnections != null && tcpConnections.Length > 0)
            {
                return TcpState.Established;
            }
            else return TcpState.Unknown; //todo
        }

       
      
 
    }
}

//  Console.WriteLine("Parallel.ForEach finished after {0} Milliseconds.\n", sw.ElapsedMilliseconds);
//(endTime - startTime).TotalMilliseconds.ToString());

/*
                sw.Restart();
                Console.WriteLine("Starting test: Task.WaitAll...");
                // PerformTest_TaskWaitAll(ModBusConnections, startTime);
                Task.WaitAll(ModBusConnections.Select(worker => worker.Value.GetRegulatorData()).ToArray());

                endTime = DateTime.Now;
                Console.WriteLine("Task.WaitAll finished after {0} Milliseconds.\n", sw.ElapsedMilliseconds);
                //    (endTime - startTime).TotalMilliseconds.ToString());


                sw.Restart();
                Console.WriteLine("Starting test: Task.WhenAll...");
                var all = Task.WhenAll(ModBusConnections.Select(worker => worker.Value.GetRegulatorData()));
                all.Wait();
                byte[][] ssa = all.Result;

                endTime = DateTime.Now;
                Console.WriteLine("Task.WhenAll finished after {0} Milliseconds.\n", sw.ElapsedMilliseconds);
                // (endTime - startTime).TotalMilliseconds.ToString());

                foreach (var r in all.Result)
                {
                    Console.WriteLine(Utilz.PrintByteArray(r) + "\n");
                }

                /*
                                // we add and remove things from list... this is safe
                                for (int i = ModBusConnections.Count - 1; i >= 0; i--)
                                {

                                    if (i == ModBusConnections.Count - 1) { sw.Restart(); }
                                    var connection = ModBusConnections[i];

                                    if (connection.ConnectionStatus == false)
                                    {
                                        var match = Program.errorConnections.Contains(connection.ListIndex);
                                        if (match == false)
                                        {
                                            if (connection.Dio == null || !connection.Dio.Connected)
                                            {
                                                Utilz.ErrorConnection(connection, "dio");
                                            }

                                            if (connection.RegulatorSync == null || !connection.RegulatorSync.Connected)
                                            {
                                                Utilz.ErrorConnection(connection, "regulator");
                                            }
                                        }
                                        continue;
                                    }
                                    // TCP SOCKET ERROR
                                    if (connection != null && connection.Dio.Connected == false)
                                    {
                                        if (connection.RegulatorSync == null)
                                        {
                                            Console.WriteLine("no TCP Client Reachable on this ip: " + connection.RegulatorIpAddress);
                                            bytesToSendToClient = Utilz.IpLenghtNormalization(connection.RegulatorIpAddress);

                                        }
                                        if (connection.Dio.Connected == false)
                                        {
                                            Console.WriteLine("no DIO Reachable on this ip: " + connection.DioIpAddress);
                                            bytesToSendToClient = Utilz.IpLenghtNormalization(connection.DioIpAddress);

                                        }

                                        // set error state
                                        //Utilz.ErrorConnection(connection, i);

                                        _client.SendTo(bytesToSendToClient, bytesToSendToClient.Length, SocketFlags.None, _ipepClient);
                                        continue; //break !! current loop                                       
                                    }

                                    //ip address, normalized for sending                                 
                                    bytesToSendToClient = Utilz.IpLenghtNormalization(connection.RegulatorIpAddress);

                                    /*
                                    * we need to make shore that 24 is number of registers to read, so in 00.060 position there is a list of parameters to read, we have 12, 
                                    * but since we have a combination of 16bit and 32 bit parameters we read them all as 32bit
                                    * hence 24 ... 16 bit = 1 number of points, 32 bit == 2 number of points
                                    * list of received bytes and order is in RX class on client
                                    */

//regulator readouts
/*
                    byte[] bytesFromEmerson = { 0x20 }; // 32 is error code
                    if (connection.RegulatorSync != null && connection.RegulatorSync.Connected == true)
                    {
                        bytesFromEmerson = connection.RegulatorSync.ReadHoldingRegistersBytes(StartingRegister, 24);
                    }


                    //REGULATOR ERROR HANDLING 
                    // 32 is random number to prevent errors
                    if (bytesFromEmerson.Length == 1 && bytesFromEmerson[0] == 32)
                    {
                        if (connection.RegulatorError++ > 2)
                        {
                            Utilz.ErrorConnection(connection, "regulator");
                        }
                        else
                        {
                            Console.WriteLine("RETRY NUMBER: " + connection.RegulatorError);
                            Console.WriteLine("tried to read,  no response from regulator TCP socket on this ip: " + connection.RegulatorIpAddress);

                            _client.SendTo(bytesToSendToClient, bytesToSendToClient.Length, SocketFlags.None, _ipepClient);
                        }
                        continue; //break current loop !!!
                    }

                    //DIO
                    bool[] DioBit = connection.Dio.ReadCoils(1, (ushort)connection.DioBit, 1);

                    new BitArray(DioBit).CopyTo(data, 0);

                    // DIO ERROR HANDLING
                    if (DioBit.Length == 8 && BitConverter.ToInt16(data, 0) == 32)
                    {

                        Console.WriteLine("tried to read, connection established but no response from DIO on this ip: " + connection.DioIpAddress + "\n\n");
                        bytesToSendToClient = Utilz.IpLenghtNormalization(connection.DioIpAddress);
                        _client.SendTo(bytesToSendToClient, bytesToSendToClient.Length, SocketFlags.None, _ipepClient);


                        Utilz.ErrorConnection(connection, "dio");

                        //  continue;
                    }

                    // ADD DIO BIT
                    Array.Resize(ref bytesToSendToClient, bytesToSendToClient.Length + 1);
                    bytesToSendToClient[bytesToSendToClient.Length - 1] = Convert.ToByte(DioBit[0]);//Convert.ToByte(1);

                    // add Emerson response 
                    bytesToSendToClient = Utilz.AppendBytes(bytesFromEmerson, bytesToSendToClient);

                    //SEND TO CLIENT -- ALL OK
                    _client.SendTo(bytesToSendToClient, bytesToSendToClient.Length, SocketFlags.None, _ipepClient);

                    Array.Clear(bytesToSendToClient, 0, bytesToSendToClient.Length);
                    connection.DioError = 0;
                    connection.RegulatorError = 0;



                    if (i == 0) { Console.WriteLine("MS ALL: " + sw.ElapsedMilliseconds); }
                }//);
*/

/*
public async Task<byte[]> DoWorkAsync(DateTime testStart)
{
    List<byte> list = new List<byte>();
    byte[] x, bytesFromEmerson = { 0x20 }; // 32 is error code;
    bool[] DBits;
    byte[] intBytes = BitConverter.GetBytes(ID);
    Array.Reverse(intBytes);
    list.AddRange(intBytes);


    //CONECTION ERROR
    if (DioConnectionOK == false || RegulatorConnectionOK == false)
    {
        var match = Program.errorConnections.Contains(ListIndex);
        if (match == false)
        {
            if (Dio == null || !Dio.Connected)
            {
                Utilz.ErrorConnection(this, "dio");
            }

            if (RegulatorSync == null || !RegulatorSync.Connected)
            {
                Utilz.ErrorConnection(this, "regulator");
            }
        }
        list.AddRange(new byte[] { 0x20 }); return list.ToArray();
    }
    // TCP SOCKET ERROR
    if (this != null && Dio.Connected == false)
    {
        list.AddRange(new byte[] { 0x20 }); return list.ToArray();
    }

    var workerStart = DateTime.Now;
    Console.WriteLine("Worker {0} started on thread {1}, beginning {2} Milliseconds after test start.",
        RegulatorIpAddress, Thread.CurrentThread.ManagedThreadId, (workerStart - testStart).TotalMilliseconds.ToString("F2"));

    if (RegulatorSync != null && RegulatorSync.Connected)
    {
        bytesFromEmerson = await RegulatorSync.ReadHoldingRegistersAsyncBytes(StartingRegister, 24);
    }

    //REGULATOR ERROR HANDLING 
    // 32 is random number to prevent errors
    if (bytesFromEmerson.Length == 1 && bytesFromEmerson[0] == 32)
    {
        if (RegulatorError++ > 2)
        {
            Utilz.ErrorConnection(this, "regulator");
        }
        else
        {
            Console.WriteLine("RETRY NUMBER: " + RegulatorError);
            Console.WriteLine("tried to read,  no response from regulator TCP socket on this ip: " + RegulatorIpAddress);

        }
        list.AddRange(new byte[] { 0x20 }); return list.ToArray();

    }


    DBits = await Task.Run(() => Dio.ReadCoils(1, (ushort)DioBit, 1));


    var workerEnd = DateTime.Now;
    Console.WriteLine("Worker {0} stopped; the worker took {1} Milliseconds, and it finished {2} Milliseconds after the test start.",
        RegulatorIpAddress, (workerEnd - workerStart).TotalSeconds.ToString("F2"), (workerEnd - testStart).TotalMilliseconds.ToString("F2"));


    byte[] fromDio = new byte[2];
    new BitArray(DBits).CopyTo(fromDio, 0);

    // DIO ERROR HANDLING
    if (DBits.Length == 8 && BitConverter.ToInt16(fromDio, 0) == 32)
    {

        Console.WriteLine("tried to read, connection established but no response from DIO on this ip: " + DioIpAddress + "\n\n");
        Utilz.ErrorConnection(this, "dio");

        list.AddRange(new byte[] { 0x20 }); return list.ToArray();
    }

    list.AddRange(fromDio);
    list.AddRange(bytesFromEmerson);


    return list.ToArray();





}

*/
