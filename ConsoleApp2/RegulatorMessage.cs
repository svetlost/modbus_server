﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleModbusServer
{
    /// <summary>
    /// Helper class <c>RegulatorMessage</c> for regulator messages
    /// </summary>
    /// <remark> this is not for dio</remark>   
    public class RegulatorMessage
    {
        ///<summary>for single regulator or for multiple regulators</summary>
        public byte CommandType { get; set; }
        
        // code String ("33.006") is transformed to code ushort 33.006  in 16bit or 32bits
        ///<summary>string that corresponds to regulator parameter code</summary>
        public string CodeString { get; set; }
        ///<summary>value to send to regulator</summary>
        public double ValueToSend { get; set; }
        ///<summary>converted code from string to ushort</summary>
        private ushort codeSend;
        
        /// <summary>
        /// depending register type sends single or multiple register write command 
        /// </summary>
        /// <param name="mbc">MotorIo</param>
        public void Send(MotorIO mbc)
        {
            if (mbc.RegulatorInputs != null)
            {
                if (CommandType == 16)
                {
                    codeSend = Utilz.Ind(CodeString, Utilz.Len.b16);
                    mbc.RegulatorInputs.WriteSingleRegisterAsync(codeSend, (ushort)ValueToSend);
                }
                if (CommandType == 32)
                {
                    codeSend = Utilz.Ind(CodeString, Utilz.Len.b32);
                    ushort[] valuesTosend = new ushort[] { (ushort)((int)ValueToSend >> 16), (ushort)((int)ValueToSend & 0x0000FFFF) };
                    mbc.RegulatorInputs.WriteMultipleRegistersAsync(codeSend, valuesTosend); // we need to write to multiple registers , Emerson has only 16bit registers
                }
            }
            else { LogWriter.LogWrite(LogWriter.Types.Fatal, $"Can't send to regulator at ip:  {mbc.RegulatorIpAddress}"); }
        }
    }
}
