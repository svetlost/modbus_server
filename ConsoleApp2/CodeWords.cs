﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Runtime.InteropServices;

namespace ConsoleModbusServer
{   
   
    /// <summary>
    /// <c>CodeWords</c> class converts values from database table to class
    /// </summary>
    [Table(Name = "code_words")]
    public class CodeWords
    {
        private int id;
        /// <summary>
        /// motor id
        /// </summary>
        [Column(Name = "id", DbType = "Int NOT NULL", IsPrimaryKey = true, IsDbGenerated = true)]
        public int ID
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }
        
        private string code;

        /// <summary>
        /// regulator parameter
        /// </summary>
        [Column(Name = "command_code")]        
        public string CommandCode
        {
            get
            {
                return this.code;
            }
            set
            {
                this.code = value;
            }
        }

        private string code_description;
        /// <summary>
        /// description from database
        /// </summary>
        [Column(Name = "code_description")]
        public string CodeDescription
        {
            get
            {
                return this.code_description;
            }
            set
            {
                this.code_description = value;
            }
        }

        private byte number_of_bits;
        /// <summary>
        /// 16 or 32 bit
        /// </summary>
        [Column(Name = "number_of_bits")]
        public byte NumberOfBits
        {
            get
            {
                return this.number_of_bits;
            }
            set
            {
                this.number_of_bits = value;
            }
        }

        private byte cyclic_read;
        /// <summary>
        /// parameter that is to be red in client rx
        /// </summary>
        [Column(Name = "cyclic_read")]
        public byte CyclicRead
        {
            get
            {
                return this.cyclic_read;
            }
            set
            {
                this.cyclic_read = value;
            }
        }
        private Nullable<Byte> code_order_in_regulator;
        /// <summary>
        /// code order in menu 22
        /// </summary>        
        [Column(Name = "code_order_in_regulator")]
        public Nullable<Byte> CodeOrderOnRegulator
        {
            get
            {
                return this.code_order_in_regulator;
            }
            set
            {
                this.code_order_in_regulator = value;
            }
        }
        private string par_name_in_app;
        /// <summary>
        /// name of regulator parameter in application
        /// </summary>
        [Column(Name = "par_name_in_app")]
        public string ParNameInApp
        {
            get
            {
                return this.par_name_in_app;
            }
            set
            {
                this.par_name_in_app = value;
            }
        }
        


    }
}
