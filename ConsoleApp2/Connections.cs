﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleModbusServer
{
    /// <summary>
    /// <c>Connections</c> class handles connection to client and regulators.
    /// Gets data from SQL server configuration tables
    /// Reconnection to regulators is done here
    /// </summary>
    public class Connections : IDisposable
    {
        /// <summary>
        /// clean up
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                Client.Close();
            }
            // free native resources
        }

        /// <summary>
        /// Timer for reconnecting regulators and dios
        /// </summary>
        public static Timer ReconnectionTimer;

        /// <summary>
        /// timer for polling data from regulators and dios
        /// </summary>
        readonly Timer MotorsDataPoller;
        long maximum = 0, min = 1000;
        readonly Stopwatch sw = new Stopwatch();
        long max = 0, maxcount = 0;

        /// <summary>
        /// socket for connecting to udp client
        /// </summary>
        Socket Client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

        /// <summary>
        /// clients ip and port
        /// </summary>
        readonly IPEndPoint IpepClient = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ClientIP), Properties.Settings.Default.ClientPort);

        /// <summary>
        /// listener for client and listening port
        /// </summary>
        static IPEndPoint IpepClientListener = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ClientIP), Properties.Settings.Default.ServerPort);

        /// <summary>
        /// list of regulator commands
        /// </summary>
        List<CodeWords> CodeWords;

        /// <summary>
        /// id and type: regulator or dio
        /// </summary>
        public static ConcurrentDictionary<int, string> errorConnections = new ConcurrentDictionary<int, string>();
        /// <summary>
        /// motor id and class
        /// </summary>
        public static ConcurrentDictionary<int, MotorIO> ModBusConnections = new ConcurrentDictionary<int, MotorIO>();

        /// <summary>
        /// list of regulator codes red from database
        /// </summary>
        public static List<string> codeList = new List<string>();

        /// <summary>
        /// interval for reconnection to regulators and dios
        /// </summary>
        public static int ReconectionInterval = System.Threading.Timeout.Infinite;

        /// <summary>
        /// interval for data polling from regulators
        /// </summary>
        static int MotorsDataPoolerInterval = Properties.Settings.Default.MotorsDataPoolerInterval;
        int EmergencyStopCounter = 0;

        /// <summary>
        /// This is class constructor
        /// starts timers for data polling from motors
        /// prepares timer for erred connections
        /// loads configuration data from database
        /// </summary>
        public Connections()
        {
            //reconnect to dio or regulator ! not started
            ReconnectionTimer = new Timer(new TimerCallback(ReconnectionTick), null, System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);

            //get data from regulators ! not started
            MotorsDataPoller = new Timer(new TimerCallback(PollAndSendSync), null, System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);

            //db read - at the end we start RegDioDataPooler timer
            DdReadAndCreateModbusConnections(new SqlConnection(Properties.Settings.Default.ServerConnectionString));

            //start listening for client UDP commands from client
            Thread oThreadone = new Thread(UDPClientCommandsForMotors);
            oThreadone.Start();
        }

        /// <summary>
        /// <c>PollAndSendSync</c> is parallel blocking method.
        /// Because of volatility of answer from all regulators timer  tick is stopped until all motors return
        /// To stop client buffer for clogging messages are sent at minimum of 50 ms
        /// message bytes are:
        /// 0. number of regulators
        /// 1-xx. list of regulators
        /// xx-yy. list of message lengths 
        /// yy-zz. messages - message length is 57 ( 14 8bit commands red as 14 32bit  commands --> 14*2*2 + dio 1bit = 56 +1)
        /// </summary>
        /// <param name="state"></param>
        public async void PollAndSendSync(object state)
        {

            if (ModBusConnections.Count == 0) return;
            // stop the timer in case response is slower then the loop
            MotorsDataPoller.Change(Timeout.Infinite, Timeout.Infinite);
            //Console.Write("\n{0:hh:mm:ss.fff} POOLED  timespan: {1}", DateTime.Now, (DateTime.Now - prevoustime).TotalMilliseconds);
            var watch = Stopwatch.StartNew();
            List<byte> ids = new List<byte>();
            List<byte> messageLengths = new List<byte>();
            List<byte> messages = new List<byte>();
            //List<byte> dios = new List<byte>();
            //  Console.WriteLine("Starting test: Parallel.ForEach...");
            ConcurrentBag<byte[]> resultCollection = new ConcurrentBag<byte[]>();


            ParallelLoopResult result = Parallel.ForEach(ModBusConnections, worker => resultCollection.Add(worker.Value.PollMotorData()));

            //check what is coming back PrintByteArray is in Garbage.cs
            foreach (var item in resultCollection)
            {
                //id
                byte[] mId = item.Take(4).ToArray();
                ids.Add(mId[0]);

                //length of message minus id
                if (item[4] == 0 || item[4] == 1)
                {
                    byte[] messageLength = BitConverter.GetBytes(item.Length - 4);
                    messageLengths.Add(messageLength[0]);
                    //message without id
                    byte[] a = item.Skip(4).ToArray();
                    //Console.WriteLine(Utilz.PrintByteArray(a));
                    messages.AddRange(a);
                    // byte[] b = Utilz.SubArray(item, 4, 1);
                    // dios.Add(b[0]);
                }
                else
                {
                    //byte[] b = Utilz.SubArray(item, 4, 1);
                    //dios.Add(b[0]);
                    messageLengths.Add(0);
                    //messages.Add(0);
                }

                //Console.WriteLine("\n" + Utilz.PrintByteArray(item) + "\n");
            }
            //Console.WriteLine("resultCollection Length:" + resultCollection.Count + "\n");

            List<byte> sendList = new List<byte>();

            byte[] id = BitConverter.GetBytes(ids.Count);
            //Array.Reverse(id);

            //DateTime ldt = DateTime.Now;
            //long binLocal = new DateTimeOffset(ldt).ToUnixTimeMilliseconds();
            //byte[] vOut = BitConverter.GetBytes(binLocal);
            //long x = BitConverter.ToInt64(vOut, 0);
            //DateTimeOffset localDate2 = DateTimeOffset.FromUnixTimeMilliseconds(binLocal);
            //DateTime localDate3 = DateTime.FromBinary(x);
            
            //number of ids / motors
            sendList.Add(id[0]);


            //time poll started , used for timing on client side 
            //sendList.AddRange(vOut);

            //list of motors
            sendList.AddRange(ids);

            //all lengths of received messages
            sendList.AddRange(messageLengths);

            //dios or error codes 32 64 96
            //sendList.AddRange(dios);

            //messages
            sendList.AddRange(messages);


            //byte[] send = resultCollection.SelectMany(x => x).ToArray();

            watch.Stop();
            //prevoustime = DateTime.Now;

            byte[] send = sendList.ToArray();

            if (watch.ElapsedMilliseconds > maximum) { maximum = watch.ElapsedMilliseconds; }
            if (watch.ElapsedMilliseconds < min) { min = watch.ElapsedMilliseconds; }

            Console.Write("\r{0:hh:mm:ss.fff} send Length : {1} elapsed time --> this run : {4} , min: {2} ms , max: {3} ms  ", DateTime.Now, send.Length, min, maximum, watch.ElapsedMilliseconds);

            if (send.Length > 4)
            {
                // poll it only once (Timeout.Infinite) and do it now
                if (watch.ElapsedMilliseconds < MotorsDataPoolerInterval)
                {
                    watch.Start();
                    await Utilz.PutTaskDelay(MotorsDataPoolerInterval - (int)watch.ElapsedMilliseconds).ConfigureAwait(true);
                    watch.Stop();
                    Console.WriteLine(" VREME : " + watch.ElapsedMilliseconds);

                }
                Client.SendTo(send, send.Length, SocketFlags.None, IpepClient);

            }
            // poll it only once (Timeout.Infinite) and do it now
            MotorsDataPoller.Change(0, Timeout.Infinite);
        }

        /// <summary>
        /// Reading from db tables motor_list and code_words;
        ///  Creating RegulatorAndDioIOsys objects - modbus connections;
        ///  compare db to regulator menu 0 command list
        /// </summary>
        /// <param name="sqlConnection">server connection string from properties</param>
        public void DdReadAndCreateModbusConnections(SqlConnection sqlConnection)
        {

            SqlDataReader dr = null;
            Console.WriteLine("Getting Connection ...");
            
            try
            {
                
                Console.WriteLine("SQL Opening Connection ...");
                LogWriter.LogWrite(LogWriter.Types.Info, "SQL Opening Connection ...");
                sqlConnection.Open();
               
                Console.WriteLine("\r\nSQL Connection successful!");
                LogWriter.LogWrite(LogWriter.Types.Info, "SQL Connection successful");

                //reading regulator config file
                using (SqlCommand cmd = new SqlCommand("SELECT id, ip_address,sbc_ip_address,sbc_modul,init_sbc_bit,sbc_bit FROM motor_list where used =1 and enabled =1 ORDER BY screen_order_number", sqlConnection))
                {
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //TCP CONNECT TO MODBUS REGULATOR and DIO
                            MotorIO mbc = new MotorIO(dr.GetInt32(0), dr.GetString(1), dr.GetString(2), dr.GetByte(3), dr.GetByte(4), dr.GetByte(5));
                        }
                    }
                    else
                    {
                        Console.WriteLine("No data found");
                        LogWriter.LogWrite(LogWriter.Types.Info, "No data found");
                    }
                }
                dr.Close();

                // reading codewords
                using (DataContext db = new DataContext(sqlConnection))
                {

                    Table<CodeWords> codeWords = db.GetTable<CodeWords>();


                    // Query for CodeWords in menu 0
                    IQueryable<CodeWords> custQuery =
                        from code in codeWords
                        orderby code.CodeOrderOnRegulator ascending
                        select code;

                    foreach (CodeWords code in custQuery)
                    {

                        if (code.CyclicRead == 1) { codeList.Add(code.CommandCode); }
                        //Console.WriteLine("command: " + code.CommandCode + " ---  bits : " + code.NumberOfBits + " --- parameter name: " + code.CodeDescription);
                        LogWriter.LogWrite(LogWriter.Types.Info,$"command: { code.CommandCode } ---  bits : { code.NumberOfBits } --- parameter name: {code.CodeDescription}");

                    }

                   // Console.WriteLine("\n\n");
                    db.Connection.Close();

                    CodeWords = custQuery.ToList();
                }

                //compare db code_words to regulator menu 0 list
                if (!Utilz.RegulatorCommandsListCheck()) 
                {
                    Console.WriteLine("Error reading or comparing regulator and database");
                    
                }


            }
            catch (SqlException e) { 
                Console.WriteLine("Error: Check Logs");
                LogWriter.LogWrite(LogWriter.Types.Fatal, e.Message + "\n" + e.StackTrace);
                
            }
            finally
            {
                if (sqlConnection != null) { sqlConnection.Close(); }

                //start pooling regulators               
                MotorsDataPoller.Change(0, MotorsDataPoolerInterval);
            }
        }

        /// <summary>
        /// non blocking
        /// Polls data from regulator and dios and sends it to ( by udp) client 
        /// uses async
        /// </summary>
        // static async void PollAndSend(object state)
        async void PollAndSendAsync(object state)
        {
            // stop the timer in case response is slower then the loop
            MotorsDataPoller.Change(Timeout.Infinite, Timeout.Infinite);
            var watch = Stopwatch.StartNew();
            // Console.Write("\n{0:hh:mm:ss.fff} POOLED  timespan: {1}",DateTime.Now, (DateTime.Now - prevoustime).TotalMilliseconds);

            List<Task<byte[]>> tasks = new List<Task<byte[]>>();

            List<byte> ids = new List<byte>();
            List<byte> messageLengths = new List<byte>();
            List<byte> messages = new List<byte>();


            foreach (MotorIO sys in ModBusConnections.Values)
            {
                tasks.Add(sys.PollMotorDataAsync());
            }

            var results = await Task.WhenAll(tasks).ConfigureAwait(true);

            foreach (var item in results)
            {
                //a.Add(item); old simple

                //id
                byte[] mId = item.Take(4).ToArray();
                ids.Add(mId[0]);

                //length of message minus id
                if (item[4] == 0 || item[4] == 1)
                {
                    byte[] messageLength = BitConverter.GetBytes(item.Length - 4);
                    messageLengths.Add(messageLength[0]);
                    //message without id
                    byte[] a = item.Skip(4).ToArray();
                    //Console.WriteLine(Utilz.PrintByteArray(a));
                    messages.AddRange(a);
                    // byte[] b = Utilz.SubArray(item, 4, 1);
                    // dios.Add(b[0]);
                }
                else
                {
                    //byte[] b = Utilz.SubArray(item, 4, 1);
                    //dios.Add(b[0]);
                    messageLengths.Add(0);
                    //messages.Add(0);
                }

            }

            // byte[] send = a.SelectMany(x => x).ToArray();

            List<byte> sendList = new List<byte>();

            byte[] id = BitConverter.GetBytes(ids.Count);
            //Array.Reverse(id);

            //DateTime ldt = DateTime.Now;
            //long binLocal = new DateTimeOffset(ldt).ToUnixTimeMilliseconds();
            //byte[] vOut = BitConverter.GetBytes(binLocal);
            //long x = BitConverter.ToInt64(vOut, 0);
            //DateTimeOffset localDate2 = DateTimeOffset.FromUnixTimeMilliseconds(binLocal);
            //DateTime localDate3 = DateTime.FromBinary(x);
            //number of ids / motors
            sendList.Add(id[0]);


            //time poll started , used for timing on client side 
            //sendList.AddRange(vOut);

            //list of motors
            sendList.AddRange(ids);

            //all lengths of received messages
            sendList.AddRange(messageLengths);

            //dios or error codes 32 64 96
            //sendList.AddRange(dios);

            //messages
            sendList.AddRange(messages);

            watch.Stop();

            //prevoustime = DateTime.Now;

            byte[] send = sendList.ToArray();

            if (watch.ElapsedMilliseconds > maximum) { maximum = watch.ElapsedMilliseconds; }
            if (watch.ElapsedMilliseconds < min && watch.ElapsedMilliseconds != 0) { min = watch.ElapsedMilliseconds; }

            //Console.Write("\r{0:hh:mm:ss.fff} send Length : {1} elapsed time --> min: {2} ms , max: {3} ms  ", DateTime.Now, send.Length, min, maximum);

            if (send.Length > 4)
            {
                //if response comes back faster wait till interval time is out
                if (watch.ElapsedMilliseconds < MotorsDataPoolerInterval)
                {
                    watch.Start();
                    await Utilz.PutTaskDelay(MotorsDataPoolerInterval - (int)watch.ElapsedMilliseconds).ConfigureAwait(true);
                    watch.Stop();
                    Console.WriteLine("UKUPNO VREME ODGOVORA : " + watch.ElapsedMilliseconds);

                }
                Client.SendTo(send, send.Length, SocketFlags.None, IpepClient);

            }
            // poll it only once (Timeout.Infinite) and do it now
            MotorsDataPoller.Change(0, Timeout.Infinite);
        }

        /// <summary>
        /// Waits for udp packages from client; 
        /// Transforms udp to commandsToSend class;
        ///  Sends commands (code, value) to regulators and dio;
        ///  To avoid error handling in lover levels , returns number 32 if something is wrong with communication or package         /// 
        /// @CommandType defined as 8(send to dio),16,32(one command to one or menu regulators) or 64( multiple commands to one or multiple regulators)
        /// logs to error log file
        /// </summary> 
        /// <exception cref="ArgumentNullException">if regulator is null</exception>
        /// <exception cref="SocketException">Tcp socket for communicating with modbus</exception>
        /// <exception cref="Exception">all other</exception>
        public void UDPClientCommandsForMotors()
        {
            int counter = 0;
            using var udpClient = new UdpClient(9056);
            while (true)
            {

                try
                {

                    //Gets the amount of data received from the network that is available to read.
                    // check if there are some regulator dio systems working 
                    if (udpClient.Available > 0)
                    {

                        EmergencyStopCounter = 0;
                        byte[] ClientMassage = udpClient.Receive(ref IpepClientListener); //toServerIpepClient

                        if (max < sw.ElapsedMilliseconds) { max = sw.ElapsedMilliseconds; }
                        if (maxcount < counter) { maxcount = counter; }
                        //Console.WriteLine("max milliseconds wait: "+ max + " ms          current ms: " + sw.ElapsedMilliseconds + "      number of loops: "+ counter + "          count loops max:" + maxcount);
                        counter = 0;
                        sw.Restart();
                        if (ClientMassage.Length > 0)
                        {

                            int codeLength = (int)ClientMassage[0];
                            ConcurrentDictionary<int, List<RegulatorMessage>> senderToRegulator = new ConcurrentDictionary<int, List<RegulatorMessage>>();
                            List<RegulatorMessage> RegulatorMessageList = new List<RegulatorMessage>();


                            // we have to know size of all parts of our udp package, so we read from the start
                            int packetOffset = 0;

                            //first byte is number of regulators to send data to
                            byte NumberOfMotors = ClientMassage[0];
                            packetOffset++;

                            //regulators to send data to
                            List<MotorIO> MotorsList = new List<MotorIO>();

                            List<byte> addressBit = new List<byte>();


                            for (int i = 0; i < NumberOfMotors; i++)
                            {
                                MotorsList.Add(ModBusConnections[ClientMassage[packetOffset]]);
                                senderToRegulator.TryAdd(ModBusConnections[ClientMassage[packetOffset]].ID, new List<RegulatorMessage>());
                                packetOffset++;
                            }


                            // DIO(8), REGULATOR SINGLE(16,32) OR MULTIPLE COMMANDS(64)
                            byte CommandType = ClientMassage[packetOffset];
                            packetOffset++;


                            //DIO, send to dio
                            if (CommandType == 8)
                            {
                                for (int i = 0; i < NumberOfMotors; i++)
                                {
                                    // init or sbc
                                    addressBit.Add(ClientMassage[packetOffset]);
                                    packetOffset++;
                                }

                                bool value = BitConverter.ToBoolean(ClientMassage, packetOffset);


                                for (int i = 0; i < MotorsList.Count; i++)
                                {
                                    MotorIO connection = MotorsList[i];

                                    if (connection.DioConnected == false) { continue; }

                                    if (connection != null)
                                    {
                                        if (connection.Dio.Connected())
                                        {
                                            //ACTUAL WRITING
                                            connection.Dio.WriteSingleCoil((byte)connection.DioModul, addressBit[i], value);
                                        }
                                        else
                                        {
                                            LogWriter.LogWrite(LogWriter.Types.Fatal, "Dio on " + connection.DioIpAddress + " is in disconnected state, check if on line");
                                            Utilz.EmergencyStop(connection.DioIpAddress);
                                        }
                                    }
                                }
                            }
                            // skipped dio, continue reading here,regulator single command, send one command (code, value) to one or menu regulators
                            else if (CommandType == 16 || CommandType == 32)
                            {
                                RegulatorMessage regSendComm = new RegulatorMessage();
                                regSendComm.CommandType = CommandType;

                                // read the length (in bytes) of string parameter
                                int codeLength5 = BitConverter.ToInt32(ClientMassage, packetOffset);
                                packetOffset += 4;

                                //code                            
                                regSendComm.CodeString = Encoding.UTF8.GetString(ClientMassage, packetOffset, codeLength5);
                                packetOffset += codeLength5;

                                //value to send
                                regSendComm.ValueToSend = BitConverter.ToDouble(ClientMassage, packetOffset); // sent as double !!

                                // for single or multiple regulators
                                foreach (MotorIO mbc in MotorsList)
                                {
                                    senderToRegulator[mbc.ID].Add(regSendComm);
                                }
                            }
                            //skipped dio and 16 or 32 continue reading here, regulator Multiple commands
                            else if (CommandType == 64)
                            {
                                // number of received commands from client
                                byte numberofcommands = ClientMassage[packetOffset];
                                packetOffset++;


                                //gets regulator commandtype and command code and puts it to the new RegulatorMessage()
                                //each command has 16 or 32 register size and command code
                                for (int i = 0; i < numberofcommands; i++)
                                {
                                    RegulatorMessage regSMessage = new RegulatorMessage();

                                    //16 or 32 bit
                                    regSMessage.CommandType = ClientMassage[packetOffset];
                                    packetOffset++;

                                    // read the length (in bytes) of string parameter
                                    int codeLength5 = BitConverter.ToInt32(ClientMassage, packetOffset);
                                    packetOffset += 4;

                                    //code                            
                                    regSMessage.CodeString = Encoding.UTF8.GetString(ClientMassage, packetOffset, codeLength5);
                                    packetOffset += codeLength5;


                                    RegulatorMessageList.Add(regSMessage);

                                }

                                byte numberOfValuesToSend = ClientMassage[packetOffset];
                                packetOffset++;

                                int index = 0, connectionIndex = 0; //connectionIndex coresponds to numberofcommands

                                //todo: remove new RegulatorMessage use existing just add value
                                for (int c = 0; c < numberOfValuesToSend; c++)
                                {
                                    RegulatorMessage regMesssage = new RegulatorMessage(); //  RegulatorMessageList[index];

                                    //value to send
                                    regMesssage.ValueToSend = BitConverter.ToDouble(ClientMassage, packetOffset); // sent as double !!                                   
                                    packetOffset += 8;

                                    if (index == numberofcommands) { index = 0; connectionIndex++; }
                                    regMesssage.CodeString = RegulatorMessageList[index].CodeString;
                                    regMesssage.CommandType = RegulatorMessageList[index].CommandType;

                                    index++;


                                    senderToRegulator[MotorsList[connectionIndex].ID].Add(regMesssage);
                                }
                            }
                            else
                            {
                                LogWriter.LogWrite(LogWriter.Types.Error, "Error receiving udp message, unknown command type :" + CommandType + " byte list from message: " + Utilz.PrintByteArray(ClientMassage));
                            }


                            // sending to regulator
                            if (senderToRegulator[MotorsList[0].ID].Count > 0)
                            {
                                sw.Restart();
                                foreach (MotorIO mbc in MotorsList)
                                // Parallel.ForEach(connections, mbc =>
                                {
                                    List<RegulatorMessage> lcms = senderToRegulator[mbc.ID];
                                    foreach (RegulatorMessage cms in lcms)
                                    {
                                        cms.Send(mbc);
                                        // Console.WriteLine("command ={0}, value={2}, ID={1}", cms.codeString, mbc.ID, cms.value);
                                    }

                                }// );
                                 // Console.WriteLine("ForEach finished after {0} Milliseconds.\n", sw.ElapsedMilliseconds);
                                RegulatorMessageList.Clear();
                                Console.WriteLine("\n\n");
                            }
                        }
                    }
                    else
                    {
                        EmergencyStopCounter++;
                        //Console.WriteLine("No connection to client part of application, stop everything");
                        if (sw.ElapsedMilliseconds > 750 && EmergencyStopCounter == 1)
                        {
                            Utilz.EmergencyStop();
                        }
                        counter++;
                    }
                }
                catch (ArgumentNullException ane)
                {
                    LogWriter.LogWrite(LogWriter.Types.Fatal, "ArgumentNullException : {0}" + "\n" + ane.StackTrace);
                }
                catch (SocketException se)
                {
                    LogWriter.LogWrite(LogWriter.Types.Fatal, "SocketException : {0}" + "\n" + se.StackTrace);
                }
                catch (Exception e)
                {
                    LogWriter.LogWrite(LogWriter.Types.Fatal, e.ToString() + "\n" + e.StackTrace);
                }
            }
        }

        private static int ReconnectionCounter = 0;
        /// <summary>
        /// timed reconnection to ip
        /// this is in separate thread
        /// loops forever, or until no error connections
        /// errorConnections is a dictionary of index numbers of existing Modbus connections, and error type, dio or regulator 
        /// </summary>
        /// <param name="state"></param>
        static void ReconnectionTick(object state)
        {
            if (errorConnections.Count == 0)
            {
                ReconectionInterval = System.Threading.Timeout.Infinite;
                ReconnectionTimer.Change(System.Threading.Timeout.Infinite, ReconectionInterval);
                ReconnectionCounter = 0;
                return;
            }
            IPAddress address, address1;
            PingReply regulator = null, dio = null;
            foreach (var val in errorConnections)
            {
                //check if ModBusConnections exists
                MotorIO mbcError = ModBusConnections.FirstOrDefault(k => k.Key == val.Key).Value;
                if (mbcError == null) { continue; }

                
                //Console.WriteLine("\rRECONNECTION at {0:hh:mm:ss.fff} type:{1} connection ip address:{2}", DateTime.Now, val.Value, (val.Value == "dio") ? ModBusConnections[val.Key].DioIpAddress : ModBusConnections[val.Key].RegulatorIpAddress);

                using Ping ping = new Ping();
                using Ping ping1 = new Ping();

                address = IPAddress.Parse(mbcError.RegulatorIpAddress);
                address1 = IPAddress.Parse(mbcError.DioIpAddress);

                //ping dio and regulator
                regulator = ping.Send(address, 1000);
                dio = ping1.Send(address1, 1000);

                if (ReconnectionCounter <= errorConnections.Count)
                {
                    LogWriter.LogWrite(LogWriter.Types.Info, String.Format("PING FOR REGULATOR ip: {1} ---> {2} dio: {3} ---> {4} {0:hh:mm:ss.fff}", DateTime.Now, mbcError.RegulatorIpAddress, (regulator.Status == IPStatus.Success) ? "success" : "fail", mbcError.DioIpAddress, (dio.Status == IPStatus.Success) ? "success" : "fail"));
                    LogWriter.LogWrite(LogWriter.Types.Info, String.Format("RECONNECTION at {0:hh:mm:ss.fff} type:{1} connection ip address:{2}", DateTime.Now, val.Value, (val.Value == "dio") ? ModBusConnections[val.Key].DioIpAddress : ModBusConnections[val.Key].RegulatorIpAddress));
                    ReconnectionCounter++;
                    //Console.WriteLine("PING FOR REGULATOR ip: {1} ---> {2} dio: {3} ---> {4} {0:hh:mm:ss.fff}", DateTime.Now, mbcError.RegulatorIpAddress, (regulator.Status == IPStatus.Success) ? "success" : "fail", mbcError.DioIpAddress, (dio.Status == IPStatus.Success) ? "success" : "fail");
                }

                // if both are working
                if (regulator.Status == IPStatus.Success && dio.Status == IPStatus.Success)
                {

                    ModBusConnections[val.Key].Reconnect(val.Value);

                    if (ModBusConnections[val.Key].DioConnected && ModBusConnections[val.Key].RegulatorConnected)
                    {
                       // Console.WriteLine("RECONECTION... Reconnection to ip: {0} Successful  ", (val.Value == "dio") ? mbcError.DioIpAddress : mbcError.RegulatorIpAddress);
                        LogWriter.LogWrite(LogWriter.Types.Info, String.Format("RECONECTION... Reconnection to ip: {0} Successful  ", (val.Value == "dio") ? mbcError.DioIpAddress : mbcError.RegulatorIpAddress));
                        string value1;
                        errorConnections.TryRemove(val.Key, out value1);
                    }
                    else
                    {
                       // Console.WriteLine("\r{0:hh:mm:ss.fff} RECONECTION... Reconnection to ip: {1} FAIL ", DateTime.Now, (val.Value == "dio") ? mbcError.DioIpAddress : mbcError.RegulatorIpAddress);
                        LogWriter.LogWrite(LogWriter.Types.Info, String.Format("r{0:hh:mm:ss.fff} RECONECTION... Reconnection to ip: {1} FAIL ", DateTime.Now, (val.Value == "dio") ? mbcError.DioIpAddress : mbcError.RegulatorIpAddress));
                    }
                }

            }
        }
        /// <summary>
        /// clean up
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
