namespace ConsoleModbusServer.IO
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Threading;
    //using LogAlertHB;
    using Message;
    using ConsoleModbusServer.Properties;

    using Unme.Common;
    using System.Collections.Generic;

    /// <summary>
    /// Modbus transport.
    /// Abstraction - http://en.wikipedia.org/wiki/Bridge_Pattern
    /// </summary>
    public abstract class ModbusTransport : IDisposable
    {
        private readonly object _syncLock = new object();
        private int _retries = Modbus.DefaultRetries;
        private int _waitToRetryMilliseconds = Modbus.DefaultWaitToRetryMilliseconds;
        private IStreamResource _streamResource;

        /// <summary>
        ///     This constructor is called by the NullTransport.
        /// </summary>
        internal ModbusTransport()
        {
        }

        internal ModbusTransport(IStreamResource streamResource)
        {
            Debug.Assert(streamResource != null, "Argument streamResource cannot be null.");

            _streamResource = streamResource;
        }

        /// <summary>
        ///     Number of times to retry sending message after encountering a failure such as an IOException,
        ///     TimeoutException, or a corrupt message.
        /// </summary>
        public int Retries
        {
            get { return _retries; }
            set { _retries = value; }
        }

        /// <summary>
        /// If non-zero, this will cause a second reply to be read if the first is behind the sequence number of the
        /// request by less than this number.  For example, set this to 3, and if when sending request 5, response 3 is
        /// read, we will attempt to re-read responses.
        /// </summary>
        public uint RetryOnOldResponseThreshold { get; set; }

        /// <summary>
        /// If set, Slave Busy exception causes retry count to be used.  If false, Slave Busy will cause infinite retries
        /// </summary>
        public bool SlaveBusyUsesRetryCount { get; set; }

        /// <summary>
        ///     Gets or sets the number of milliseconds the tranport will wait before retrying a message after receiving
        ///     an ACKNOWLEGE or SLAVE DEVICE BUSY slave exception response.
        /// </summary>
        public int WaitToRetryMilliseconds
        {
            get { 
                return _waitToRetryMilliseconds; 
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException(Resources.WaitRetryGreaterThanZero);

                _waitToRetryMilliseconds = value;
            }
        }

        /// <summary>
        ///     Gets or sets the number of milliseconds before a timeout occurs when a read operation does not finish.
        /// </summary>
        public int ReadTimeout
        {
            get { return StreamResource.ReadTimeout; }
            set { StreamResource.ReadTimeout = value; }
        }

        /// <summary>
        ///     Gets or sets the number of milliseconds before a timeout occurs when a write operation does not finish.
        /// </summary>
        public int WriteTimeout
        {
            get { return StreamResource.WriteTimeout; }
            set { StreamResource.WriteTimeout = value; }
        }

        /// <summary>
        ///     Gets the stream resource.
        /// </summary>
        internal IStreamResource StreamResource
        {
            get { return _streamResource; }
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<Pending>")]
        internal virtual T UnicastMessage<T>(IModbusMessage message) where T : IModbusMessage, new()
        {
            MotorIO connection = null;
            IModbusMessage response = null;
            int attempt = 1;
            bool success = false;
            string ip = "";
            string type = "";
            if (this.StreamResource.tcpClient != null && this.StreamResource.tcpClient.Client != null)
            {
                ip = ((IPEndPoint)(this.StreamResource.tcpClient.Client.RemoteEndPoint)).Address.ToString();

                // find if address is dio or regular
                MotorIO a = Connections.ModBusConnections.Values.Where(x => x.RegulatorIpAddress == ip).FirstOrDefault();
                MotorIO b = Connections.ModBusConnections.Values.Where(q => q.DioIpAddress == ip).FirstOrDefault();

                // indexDio = Program.ModBusConnections.Where(x => x.Value.DioIpAddress == ip).Select(entry => entry.Key).First();

                if (a != null) { type = "regulator"; connection = a; }
                if (b != null) { type = "dio"; connection = b; }
            }
            else
            {
                LogWriter.LogWrite(LogWriter.Types.Fatal, "TCP client error or TCP disconnected");
            }
            if (connection != null)
            {

                do
                {
                    try
                    {
                        lock (_syncLock)
                        {


                            if (this.StreamResource != null && this.StreamResource.tcpClient != null && this.StreamResource.tcpClient.Client != null)
                                Write(message);
                            else
                                throw new System.ArgumentException("TCP client is disconnected");

                            bool readAgain;
                            do
                            {
                                readAgain = false;
                                response = ReadResponse<T>();

                                if (response is SlaveExceptionResponse exceptionResponse)
                                {
                                    // if SlaveExceptionCode == ACKNOWLEDGE we retry reading the response without resubmitting request
                                    readAgain = exceptionResponse.SlaveExceptionCode == Modbus.Acknowledge;
                                    if (readAgain)
                                    {
                                        Console.WriteLine(
                                            "Received ACKNOWLEDGE slave exception response, waiting {0} milliseconds and retrying to read response.",
                                            _waitToRetryMilliseconds);
                                        Sleep(WaitToRetryMilliseconds);
                                    }
                                    else
                                    {
                                        throw new SlaveException(exceptionResponse);
                                    }
                                }
                                else if (ShouldRetryResponse(message, response))
                                {
                                    readAgain = true;
                                }
                            } while (readAgain);
                        }

                        ValidateResponse(message, response);
                        success = true;


                    }

                    //TCP socket is working but regulator or dio module are not responding properly
                    catch (SlaveException se)
                    {

                        switch (se.SlaveExceptionCode)
                        {
                            case Modbus.IllegalFunction:
                                LogWriter.LogWrite(LogWriter.Types.Fatal, "Illegal Function on: " + type +
                                    "\nThe function code received in the query is not an allowable action for the slave." +
                                    "\nThis may be because the function code is only applicable to newer devices," +
                                    "\nand was not implemented in the unit selected." +
                                    "\nIt could also indicate that the slave is in the wrong state to process a request of this type," +
                                    "\nfor example because it is unconfigured and is being asked to return register values." +
                                    "\nIf a Poll Program Complete command was issued, this code indicates that no program function preceded it." +
                                    "\nip:" + this.StreamResource.tcpClient.Client.RemoteEndPoint);
                                break;
                            case Modbus.IllegalDataAddress:
                                LogWriter.LogWrite(LogWriter.Types.Fatal, "Illegal Data Address on: " + type +
                                    "\nThe data address received in the query is not an allowable address for the slave." +
                                    "\nMore specifically, the combination of reference number and transfer length is invalid. " +
                                    "\nFor a controller with 100 registers, a request with offset 96 and length 4 would succeed, " +
                                    "\na request with offset 96 and length 5 will generate exception 02." +
                                    "\na SIMPLY PUT you are trying to read more or less than you have specified," +
                                    "\ncheck number of points you are trying to read, check 16 or 32 bit." +
                                    "\nip: " + this.StreamResource.tcpClient.Client.RemoteEndPoint);
                                break;
                            case Modbus.IllegalDataValue:
                                LogWriter.LogWrite(LogWriter.Types.Fatal, "Illegal Data Value on: " + type +
                                    "\nA value contained in the query data field is not an allowable value for the slave." +
                                    "\nThis indicates a fault in the structure of remainder of a complex request," +
                                    "\nsuch as that the implied length is incorrect." +
                                    "\nIt specifically does NOT mean that a data item submitted for storage in a register has a value outside the expectation of the application program," +
                                    "\nsince the MODBUS protocol is unaware of the significance of any particular value of any particular register." +
                                    "\nip: " + this.StreamResource.tcpClient.Client.RemoteEndPoint);
                                break;
                            case Modbus.SlaveDeviceFailure:
                                LogWriter.LogWrite(LogWriter.Types.Fatal, "Slave Device Failure on: " + type +
                                    "\nAn unrecoverable error occurred while the slave was attempting to perform the requested action." +
                                    "\nip:" + this.StreamResource.tcpClient.Client.RemoteEndPoint);
                                break;
                            case Modbus.Acknowledge:
                                LogWriter.LogWrite(LogWriter.Types.Fatal, "Acknowledge on: " + type +
                                    "\nSpecialized use in conjunction with programming commands." +
                                    "\nThe slave has accepted the request and is processing it, " +
                                    "\nbut a long duration of time will be required to do so." +
                                    "\nThis response is returned to prevent a timeout error from occurring in the regulator." +
                                    "\nThe regulator can next issue a Poll Program Complete message to determine if processing is completed." +
                                    "\nip:" + this.StreamResource.tcpClient.Client.RemoteEndPoint);
                                break;
                            case Modbus.SlaveDeviceBusy:
                                LogWriter.LogWrite(LogWriter.Types.Fatal, "Slave Device Busy on: " + type +
                                    "\nSpecialized use in conjunction with programming commands." +
                                    "\nThe slave is engaged in processing a long - duration program command." +
                                    "\nThe regulator should retransmit the message later when the slave is free.." +
                                    "\nip:" + this.StreamResource.tcpClient.Client.RemoteEndPoint);
                                break;
                            case Modbus.NegativeAcknowledge:
                                LogWriter.LogWrite(LogWriter.Types.Fatal, "Negative Acknowledge on: " + type +
                                    "\nThe slave cannot perform the program function received in the query." +
                                    "\nThis code is returned for an unsuccessful programming request using function code 13 or 14 decimal." +
                                    "\nThe regulator should request diagnostic or error information from the slave." +
                                    "\nip:" + this.StreamResource.tcpClient.Client.RemoteEndPoint);
                                break;
                            case Modbus.MemoryParityError:
                                LogWriter.LogWrite(LogWriter.Types.Fatal, "Memory Parity Error on: " + type +
                                    "\nSpecialized use in conjunction with function codes 20 and 21 and reference type 6," +
                                    "\nto indicate that the extended file area failed to pass a consistency check." +
                                    "\nThe slave attempted to read extended memory or record file," +
                                    "\nbut detected a parity error in memory." +
                                    "\nThe regulator can retry the request, but service may be required on the slave device." +
                                    "\nip:" + this.StreamResource.tcpClient.Client.RemoteEndPoint);
                                break;
                            case Modbus.GatewayPathUnavailable:
                                LogWriter.LogWrite(LogWriter.Types.Fatal, "Gateway Path Unavailable on: " + type +
                                    "\nSpecialized use in conjunction with gateways," +
                                    "\nindicates that the gateway was unable to allocate an internal communication path from the input port to the output port for processing the request." +
                                    "\nUsually means the gateway is misconfigured or overloaded." +
                                    "\nip:" + this.StreamResource.tcpClient.Client.RemoteEndPoint);
                                break;
                            case Modbus.GatewayTargetDeviceFailedtoRespond:
                                LogWriter.LogWrite(LogWriter.Types.Fatal, "Gateway Target Device Failed to Respond on: " + type +
                                    "\nSpecialized use in conjunction with gateways," +
                                    "\nindicates that no response was obtained from the target device." +
                                    "\nUsually means that the device is not present on the network." +
                                    "\nip:" + this.StreamResource.tcpClient.Client.RemoteEndPoint);
                                break;
                            default:
                                LogWriter.LogWrite(LogWriter.Types.Fatal, se.Message + " exception code: " + se.SlaveExceptionCode + " ip:" + this.StreamResource.tcpClient.Client.RemoteEndPoint + " type: " + type); // to many memory addresses requested for reading --- one issue
                                break;
                        }

                        if (StreamResource.tcpClient != null)
                        {
                            this.StreamResource.tcpClient.Client.Close(); //same as dispose
                            this.StreamResource.tcpClient.Dispose();
                        }
                            this._streamResource.Dispose();
                        
                        Utilz.ErrorConnection(connection, type);


                        success = true;
                        break;

                        Console.WriteLine(
                            "Received exception response, waiting {0} milliseconds and resubmitting request. Attempt number : " + attempt,
                            _waitToRetryMilliseconds);
                        Sleep(WaitToRetryMilliseconds);
                    }

                    // socket error
                    catch (Exception e)
                    {
                        if (e is FormatException ||
                            e is NotImplementedException ||
                            e is TimeoutException ||
                            e is IOException ||
                            e is InvalidOperationException
                            )
                        {
                            LogWriter.LogWrite(LogWriter.Types.Fatal, $"ModbusTransport ip: {ip}, \n  {e.GetType().Name},\n  {e.Message},\n data: {e.Data} \n {e.StackTrace}");
                        }
                        else
                        {
                            if (this.StreamResource != null && this.StreamResource.tcpClient != null)
                            {
                                LogWriter.LogWrite(LogWriter.Types.Fatal, e.Message + " exception code: " + e.GetType().Name + " ip:" + this.StreamResource.tcpClient.Client.RemoteEndPoint + "\nstack trace: " + e.StackTrace); // to many memory addresses requsted for reading --- one issue                                
                            }
                            else { LogWriter.LogWrite(LogWriter.Types.Fatal, e.Message + " exception code: " + e.GetType().Name + "\nstack trace: " + e.StackTrace); }
                        }

                        if (this.StreamResource != null && this.StreamResource.tcpClient != null)
                        {
                            this.StreamResource.tcpClient.Client.Close();
                            this.StreamResource.tcpClient.Dispose();
                            this._streamResource.Dispose();
                        }


                        Utilz.ErrorConnection(connection, type);

                        break; // stop trying to connect to ip !!

                    }
                    finally
                    {

                    }
                } while (!success);


            }
            else
            {
                if (string.IsNullOrEmpty(ip))
                {
                    LogWriter.LogWrite(LogWriter.Types.Fatal, "TCP client error sending Unicast Message cant be performed, No active TCP Connection, ip empty ");  // No active TCP Connection to hostName:port
                }
                else
                {
                    LogWriter.LogWrite(LogWriter.Types.Fatal, "ip: " + ip + " is not found in the list");  // No active TCP Connection to hostName:port
                }
            }


            if (response == null) { return new T(); }
            if (response.GetType().Name == "SlaveExceptionResponse") { return new T(); }
            else return (T)response;
        }

        internal virtual IModbusMessage CreateResponse<T>(byte[] frame) where T : IModbusMessage, new()
        {
            byte functionCode = frame[1];
            IModbusMessage response;

            // check for slave exception response else create message from frame
            if (functionCode > Modbus.ExceptionOffset)
                response = ModbusMessageFactory.CreateModbusMessage<SlaveExceptionResponse>(frame);
            else
                response = ModbusMessageFactory.CreateModbusMessage<T>(frame);

            return response;
        }

        internal void ValidateResponse(IModbusMessage request, IModbusMessage response)
        {
            // always check the function code and slave address, regardless of transport protocol
            if (request.FunctionCode != response.FunctionCode)
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Received response with unexpected Function Code. Expected {0}, received {1}.", request.FunctionCode,
                    response.FunctionCode));

            if (request.SlaveAddress != response.SlaveAddress)
                throw new IOException(String.Format(CultureInfo.InvariantCulture,
                    "Response slave address does not match request. Expected {0}, received {1}.", response.SlaveAddress,
                    request.SlaveAddress));

            // message specific validation
            if (request is IModbusRequest req)
            {
                req.ValidateResponse(response);
            }

            OnValidateResponse(request, response);
        }

        /// <summary>
        /// Check whether we need to attempt to read another response before processing it (e.g. response was from previous request)
        /// </summary>
        internal bool ShouldRetryResponse(IModbusMessage request, IModbusMessage response)
        {
            // These checks are enforced in ValidateRequest, we don't want to retry for these
            if (request.FunctionCode != response.FunctionCode) { return false; }
            if (request.SlaveAddress != response.SlaveAddress) { return false; }
            return OnShouldRetryResponse(request, response); ;
        }

        /// <summary>
        /// Provide hook to check whether receiving a response should be retried
        /// </summary>
        internal virtual bool OnShouldRetryResponse(IModbusMessage request, IModbusMessage response)
        {
            return false;
        }

        /// <summary>
        ///     Provide hook to do transport level message validation.
        /// </summary>
        internal abstract void OnValidateResponse(IModbusMessage request, IModbusMessage response);

        internal abstract byte[] ReadRequest();

        internal abstract IModbusMessage ReadResponse<T>() where T : IModbusMessage, new();

        internal abstract byte[] BuildMessageFrame(IModbusMessage message);

        internal abstract void Write(IModbusMessage message);

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing">
        ///     <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only
        ///     unmanaged resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                DisposableUtility.Dispose(ref _streamResource);
        }

        private static void Sleep(int millisecondsTimeout)
        {
            Thread.Sleep(millisecondsTimeout);
        }
    }
}
