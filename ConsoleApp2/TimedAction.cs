﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ConsoleModbusServer
{/// <summary>
/// Delay a send
/// </summary>
    public static class TimedAction
    {/// <summary>
     /// Executes delegate with delay  
     /// </summary>
     /// <param name="action">delegate</param>
     /// <param name="delay">time in ms</param>
        public static void ExecuteWithDelay(Action action, TimeSpan delay)
        {
            DispatcherTimer timer = new DispatcherTimer
            {
                Interval = delay,
                Tag = action
            };
            timer.Tick += new EventHandler(Timer_Tick);
            timer.Start();
        }

        static void Timer_Tick(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            Action action = (Action)timer.Tag;

            action.Invoke();
            timer.Stop();
        }
    }
}
