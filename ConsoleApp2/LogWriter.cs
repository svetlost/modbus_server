﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.IO.Compression;
using System.Threading;
using System.Net;

namespace ConsoleModbusServer
{
    /// <summary>
    /// <c>LogWriter</c> class creates logs and zip files
    /// </summary>
    /// <remarks>zip files are created after Properties.Settings.Default.LogTextFileSize exceeds size </remarks>
    /// <remarks>ftp password is hardcoded</remarks>
    /// <remarks>log file is log.txt</remarks>
    public class LogWriter
    {

        private static object fileLock = new Object();
        ///<summary>path to file</summary>
        private static string m_exePath = string.Empty;
        ///<summary>timer that works once a day</summary>
        private static Timer timer;
        ///<summary>message severity</summary>
        public enum Types { Debug, Info, Warn, Error, Fatal }
        /// <summary>
        /// Creates log file. Creates zip directory and zip files. 
        /// </summary> 
        /// <remarks>
        /// If file is larger then  Properties.Settings.Default.LogTextFileSize creates zip file out of it
        /// </remarks>
        /// <param name="tip">message severity</param>
        /// <param name="logMessage">message</param>
        public static void LogWrite(Types tip, string logMessage)
        {
            //once a day
            timer = new Timer(new TimerCallback(TimerTask), null, 0, 86400000);

            m_exePath = Path.GetDirectoryName(Properties.Settings.Default.LogDirectoryLocation);
            string fileName = m_exePath + "\\" + "log.txt";
            FileInfo fi = new FileInfo(fileName);
            if (!fi.Exists)
            { 
                Directory.CreateDirectory(m_exePath);
                using FileStream fs = new FileStream(fileName, FileMode.Create);
                fi = new FileInfo(fileName);
            }

            long size = fi.Length;
            ///<value>zip file is created with date time added</value>
            string newfilename = "log_" + DateTime.Now.ToString("dd-M-yyyy");
            if (size / 1024 > Properties.Settings.Default.LogTextFileSize)
            {
                File.Move(fileName, m_exePath + "\\" + newfilename + ".txt");

                if (!Directory.Exists(m_exePath + "\\zips"))
                {
                    Directory.CreateDirectory(m_exePath + "\\zips");
                }

                string zipname = m_exePath + "\\zips\\" + newfilename + ".zip";
                using (FileStream fs = new FileStream(zipname, FileMode.Create))
                using (ZipArchive arch = new ZipArchive(fs, ZipArchiveMode.Create))
                {
                    arch.CreateEntryFromFile(m_exePath + "\\" + newfilename + ".txt", newfilename + ".txt");
                }
                FileInfo fi1 = new FileInfo(m_exePath + "\\" + newfilename + ".txt");
                fi1.Delete();

            }


            try
            {
                //This means only 1 thread at any given time can be writing to the file,
                //other threads are blocked until the current thread has exited the critical section (at which point the file lock will have been removed).
                lock (fileLock)
                {
                    using (StreamWriter w = File.AppendText(m_exePath + "\\" + "log.txt"))
                    {
                        Log(tip, logMessage, w);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Logs :" + ex.Message);
                Console.WriteLine("Logs :" + ex.StackTrace);
            }
        }
        /// <summary>
        /// uploads zip files to ftp server
        /// </summary>
        /// <param name="timerState">call from timer</param>
        private static void TimerTask(object timerState)
        {
            if (!Directory.Exists(m_exePath + "\\zip"))
            {
                Directory.CreateDirectory(m_exePath + "\\zip");
            }
            string[] fileEntries = Directory.GetFiles(m_exePath + "\\zip");
            using (var client = new WebClient())
            {

                foreach (string fileName in fileEntries)
                {
                    client.Credentials = new NetworkCredential(Properties.Settings.Default.ftpUser, "pass");
                    client.UploadFile(Properties.Settings.Default.FtpFileServer + fileName + ".zip", WebRequestMethods.Ftp.UploadFile, fileName);
                }
            }
        }
        /// <summary>
        /// adds new entry to log file
        /// </summary>
        /// <param name="tip">enum</param>
        /// <param name="logMessage">message to write</param>
        /// <param name="txtWriter"></param>
        public static void Log(Types tip, string logMessage, TextWriter txtWriter)
        {
            try
            {
              
                    string fixedmesage = logMessage.Replace(",", " - ");
                    txtWriter.WriteLine("{2}, {0:dd/MM/yy hh:mm:ss.fff} , {1}", DateTime.Now, fixedmesage, tip);
               
            }
            catch (Exception ex)
            {
                Console.WriteLine("Logs :" + ex.Message);
                Console.WriteLine("Logs :" + ex.StackTrace);
            }
        }
    }
}
